# Kaleidoscope in Mu

This project contains a compiler for the [Kaleidoscope language](https://llvm.org/docs/tutorial/MyFirstLanguageFrontend/index.html) written in Rust.
The compiler has three "backends":
 1) LLVM (using the crate `llvm-sys`)
 2) Mu (using the fast implementation [Zebu](https://gitlab.anu.edu.au/mu/mu-impl-fast))
 3) Mu/LLVM (an implementation of Zebu that uses LLVM as an [AOT Compiler](https://gitlab.anu.edu.au/u1075163/mu-aot-llvm))

Note that this project implements the following features of Kaleidoscope:
   - Arithmetic expressions
   - Types (double, int, boolean)
   - Control Flow (for loops, and if expressions)
   - Mutable Variables
   - Global Variables
   - Arrays
   
Also, note that this project uses a standard handwritten lexer and parser as instructed in the language tutorial. 
It could be further simplified by using a parser generator instead. 