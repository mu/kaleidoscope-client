# Compute the x'th fibonacci number.
def double fib(double x)
  if x < 3.0 then
    1.0
  else
    fib(x-1.0)+fib(x-2.0)

# This expression will compute the 10th number.
fib(10.0)