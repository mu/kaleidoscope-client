use crate::generator::{create_block_alloca, Context};
use crate::mu_generator::*;
use llvm_sys::core::*;
use llvm_sys::prelude::{LLVMBool, LLVMValueRef};
use llvm_sys::LLVMRealPredicate::{LLVMRealOLT, LLVMRealONE};
use llvm_sys::{LLVMModule, LLVMValue};
use mu::vm::VM;
use std::ffi::CString;
use std::os::raw::{c_char, c_double, c_uint};

#[macro_use]
use crate::ir_macros;

use crate::ast::RefType::Ref;
use crate::ast::Type::Double;
use llvm_sys::LLVMIntPredicate::{LLVMIntNE, LLVMIntULT};
use mu::ast::inst::Instruction_;
use mu::ast::inst::*;
use mu::ast::ir::*;
use mu::ast::op::BinOp;
use mu::ast::op::CmpOp;
use mu::ast::types::*;
use mu::runtime::*;
use mu::utils::LinkedHashMap;
use mu::vm::*;
use mu_aot_llvm::instr::gen_instr_branch2;
use std::borrow::Borrow;
use std::collections::hash_map::RandomState;
use std::collections::HashMap;
use std::ops::Add;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Type {
    Bool,
    Int,
    Double,
    Void,
    IntArray(i64),
    DoubleArray(i64),
    BoolArray(i64),

    // Type for anonymous functions to be resolved at runtime
    Annon
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum RefType {
    Ref(Type),
    IRef(Type)
}

#[derive(Clone)]
pub struct RealLiteral {
    val: f64
}

impl RealLiteral {
    pub fn val(&self) -> f64 {
        self.val
    }

    pub fn new(val: f64) -> RealLiteral {
        RealLiteral { val }
    }
}

#[derive(Clone)]
pub struct IntLiteral {
    val: i64
}

impl IntLiteral {
    pub fn val(&self) -> i64 {
        self.val
    }

    pub fn new(val: i64) -> IntLiteral {
        IntLiteral { val }
    }
}

#[derive(Clone)]
pub struct BoolLiteral {
    val: bool
}

impl BoolLiteral {
    pub fn val(&self) -> bool {
        self.val
    }

    pub fn new(val: bool) -> BoolLiteral {
        BoolLiteral { val }
    }
}

#[derive(Clone)]
pub struct Var {
    name: String
}

impl Var {
    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn new(name: String) -> Var {
        Var { name }
    }
}

#[derive(Clone)]
pub struct CallExp {
    callee: String,
    args: Vec<Box<dyn Exp>>
}

impl CallExp {
    pub fn callee(&self) -> String {
        self.callee.clone()
    }

    pub fn args(&self) -> &Vec<Box<dyn Exp>> {
        &self.args
    }

    pub fn new(callee: String, args: Vec<Box<dyn Exp>>) -> CallExp {
        CallExp { callee, args }
    }
}

#[derive(Clone)]
pub struct ForExp {
    var: Var,
    var_type: Type,
    start_exp: Box<dyn Exp>,
    end_exp: Box<dyn Exp>,
    step_exp: Box<dyn Exp>,
    body_exp: Box<dyn Exp>
}

impl ForExp {
    pub fn var(&self) -> &Var {
        &self.var
    }

    pub fn start_exp(&self) -> &Box<dyn Exp> {
        &self.start_exp
    }

    pub fn end_exp(&self) -> &Box<dyn Exp> {
        &self.end_exp
    }

    pub fn step_exp(&self) -> &Box<dyn Exp> {
        &self.step_exp
    }

    pub fn body_exp(&self) -> &Box<dyn Exp> {
        &self.body_exp
    }

    pub fn new(
        var: Var,
        var_type: Type,
        start_exp: Box<dyn Exp>,
        end_exp: Box<dyn Exp>,
        step_exp: Box<dyn Exp>,
        body_exp: Box<dyn Exp>
    ) -> ForExp {
        ForExp {
            var,
            var_type,
            start_exp,
            end_exp,
            step_exp,
            body_exp
        }
    }
}

#[derive(Clone)]
pub struct ConditionalExp {
    cond: Box<dyn Exp>,
    then_branch: Box<dyn Exp>,
    else_branch: Box<dyn Exp>
}

impl ConditionalExp {
    pub fn cond(&self) -> &Box<dyn Exp> {
        &self.cond
    }

    pub fn then_branch(&self) -> &Box<dyn Exp> {
        &self.then_branch
    }

    pub fn else_branch(&self) -> &Box<dyn Exp> {
        &self.else_branch
    }

    pub fn new(
        cond: Box<dyn Exp>,
        then_branch: Box<dyn Exp>,
        else_branch: Box<dyn Exp>
    ) -> ConditionalExp {
        ConditionalExp {
            cond,
            then_branch,
            else_branch
        }
    }
}

#[derive(Clone)]
pub struct BinaryExp {
    c: char,
    lhs: Box<dyn Exp>,
    rhs: Box<dyn Exp>
}

impl BinaryExp {
    pub fn c(&self) -> char {
        self.c
    }

    pub fn lhs(&self) -> &Box<dyn Exp> {
        &self.lhs
    }

    pub fn rhs(&self) -> &Box<dyn Exp> {
        &self.rhs
    }

    pub fn new(c: char, lhs: Box<dyn Exp>, rhs: Box<dyn Exp>) -> BinaryExp {
        BinaryExp { c, lhs, rhs }
    }
}

#[derive(Clone)]
pub struct VarExp {
    vars: Vec<(Type, Var, Box<dyn Exp>)>,
    body_exp: Box<dyn Exp>
}

impl VarExp {
    pub fn vars(&self) -> &Vec<(Type, Var, Box<dyn Exp>)> {
        &self.vars
    }

    pub fn body_exp(&self) -> &Box<dyn Exp> {
        &self.body_exp
    }

    pub fn new(
        vars: Vec<(Type, Var, Box<dyn Exp>)>,
        body_exp: Box<dyn Exp>
    ) -> VarExp {
        VarExp { vars, body_exp }
    }
}

#[derive(Clone)]
pub struct ArrayLiteral {
    exps: Vec<Box<dyn Exp>>
}

impl ArrayLiteral {
    pub fn exps(&self) -> &Vec<Box<dyn Exp>> {
        &self.exps
    }

    pub fn new(exps: Vec<Box<dyn Exp>>) -> ArrayLiteral {
        ArrayLiteral { exps }
    }
}

#[derive(Clone)]
pub struct ArrayAccess {
    var: String,
    idx: Box<dyn Exp>
}

impl ArrayAccess {
    pub fn var(&self) -> &String {
        &self.var
    }

    pub fn idx(&self) -> &Box<dyn Exp> {
        &self.idx
    }

    pub fn new(var: String, idx: Box<dyn Exp>) -> ArrayAccess {
        ArrayAccess { var, idx }
    }
}

#[derive(Clone)]
pub struct Prototype {
    name: String,
    return_type: Type,
    args: Vec<(Type, String)>
}

impl Prototype {
    pub fn print(&self) -> String {
        format!(
            "Prototype(({:?}, {}), {:?})",
            self.return_type, self.name, self.args
        )
    }

    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn return_type(&self) -> &Type {
        &self.return_type
    }

    pub fn replace_annon_type(&mut self, t: Type) {
        self.return_type = t
    }

    pub fn args(&self) -> &Vec<(Type, String)> {
        &self.args
    }

    pub fn new(
        name: String,
        return_type: Type,
        args: Vec<(Type, String)>
    ) -> Prototype {
        Prototype {
            name,
            return_type,
            args
        }
    }
}

#[derive(Clone)]
pub struct Function {
    proto: Box<Prototype>,
    body: Box<dyn Exp>
}

impl Function {
    pub fn print(&self) -> String {
        format!("Function({}, {})", self.proto.print(), self.body.print())
    }

    pub fn proto(&mut self) -> &mut Box<Prototype> {
        &mut self.proto
    }

    pub fn body(&self) -> &Box<dyn Exp> {
        &self.body
    }

    pub fn new(proto: Box<Prototype>, body: Box<dyn Exp>) -> Function {
        Function { proto, body }
    }
}

#[derive(Clone)]
pub struct Global {
    var: Var,
    var_type: Type,
    value: Box<dyn Exp>
}

impl Global {
    pub fn print(&self) -> String {
        format!(
            "GlobalVar({:?}, {}, {})",
            self.var_type,
            self.var.print(),
            self.value.print()
        )
    }

    pub fn var(&self) -> &Var {
        &self.var
    }

    pub fn var_type(&self) -> &Type {
        &self.var_type
    }

    pub fn value(&self) -> &Box<dyn Exp> {
        &self.value
    }

    pub fn new(var: Var, var_type: Type, value: Box<dyn Exp>) -> Global {
        Global {
            var,
            var_type,
            value
        }
    }
}

pub trait Exp: ExpClone {
    fn print(&self) -> String;
    unsafe fn code_gen(
        &self,
        module: &*mut LLVMModule,
        context: &mut Context
    ) -> Result<LLVMValueRef, String>;
    unsafe fn code_gen_mu(
        &self,
        vm: &mut Mu_VM,
        fun_ver: &mut MuFunctionVersion
    ) -> Result<P<TreeNode>, String>;
    fn get_type(
        &self,
        var_type_table: &HashMap<String, Type>,
        fn_type_table: &HashMap<String, Type>
    ) -> Type;
}

pub trait ExpClone {
    fn clone_box(&self) -> Box<dyn Exp>;
}

impl Clone for Box<dyn Exp> {
    fn clone(&self) -> Box<dyn Exp> {
        self.clone_box()
    }
}

impl<T> ExpClone for T
where
    T: 'static + Exp + Clone
{
    fn clone_box(&self) -> Box<dyn Exp> {
        Box::new(self.clone())
    }
}

impl Exp for ArrayAccess {
    fn print(&self) -> String {
        format!("ArrayAccess({}, {:?})", self.var, self.idx.print())
    }

    unsafe fn code_gen(
        &self,
        module: &*mut LLVMModule,
        context: &mut Context
    ) -> Result<*mut LLVMValue, String> {
        let array_idx: *mut LLVMValue =
            match self.idx().code_gen(module, context) {
                Ok(result) => result,
                e @ Err(_) => return e
            };

        let type_x =
            context.var_type_map.get(self.var.as_str()).unwrap().clone();

        match context.named_values.get(self.var()) {
            Some(mut val) => {
                let name = format!("{}\0", self.var.clone());

                let mut val = *val;

                let idx;

                // need to load the pointer first
                if type_x == Type::IntArray(-1) {
                    val = LLVMBuildLoad(
                        context.builder,
                        val,
                        name.as_str().as_ptr() as *const c_char
                    );

                    let pos = [array_idx];

                    idx = LLVMBuildInBoundsGEP(
                        context.builder,
                        val,
                        pos.as_ptr() as *mut LLVMValueRef,
                        1,
                        "tmp\0".as_ptr() as *const c_char
                    );
                } else {
                    let pos = [
                        LLVMConstInt(
                            context.get_llvm_type(&Type::Int),
                            0,
                            false as i32
                        ),
                        array_idx
                    ];

                    idx = LLVMBuildInBoundsGEP(
                        context.builder,
                        val,
                        pos.as_ptr() as *mut LLVMValueRef,
                        2,
                        "tmp\0".as_ptr() as *const c_char
                    );
                }

                let var = LLVMBuildLoad(
                    context.builder,
                    idx,
                    "tmp\0".as_ptr() as *const c_char
                );
                Ok(var)
            }
            None => unimplemented!()
        }
    }

    unsafe fn code_gen_mu(
        &self,
        vm: &mut Mu_VM,
        fun_ver: &mut MuFunctionVersion
    ) -> Result<Arc<TreeNode>, String> {
        let array_idx = match self.idx().code_gen_mu(vm, fun_ver) {
            Ok(result) => result,
            e @ Err(_) => return e
        };

        let iref_var_name = vm.generate_name(format!("{}_iref", self.var));
        let result_var_name = vm.generate_name(self.var.clone());

        let current_block = vm.blocks_map.get_mut(&vm.curr_block).unwrap();
        let result_type = &self
            .get_type(&current_block.var_type_map, &vm.fn_type_map)
            .clone();

        let var_type =
            &current_block.var_type_map.get(self.var()).unwrap().clone();

        let iref_var_type = match result_type {
            t => vm.get_mu_ref_type(&RefType::IRef(t.clone()))
        };

        let mu_var_type = vm.get_mu_type(&result_type);

        let current_block = vm.blocks_map.get_mut(&vm.curr_block).unwrap();
        let r = current_block.store.get(self.var.as_str());
        match r {
            Some(v) => {
                let iref_var = fun_ver.new_ssa(
                    MuEntityHeader::named(
                        vm.vm.next_id(),
                        Arc::new(iref_var_name)
                    ),
                    iref_var_type
                );

                vm.vm.set_name(iref_var.as_entity());

                let is_array = match var_type {
                    Type::IntArray(-1)
                    | Type::DoubleArray(-1)
                    | Type::BoolArray(-1) => false,
                    _ => true
                };

                if is_array {
                    let get_element_iref = fun_ver.new_inst(Instruction {
                        hdr: MuEntityHeader::unnamed(vm.vm.next_id()),
                        value: Some(vec![iref_var.clone_value()]),
                        ops: {
                            let mut ops = vec![v.clone(), array_idx.clone()];
                            ops
                        },
                        v: {
                            let inst = Instruction_::GetElementIRef {
                                is_ptr: false,
                                base: 0,
                                index: 1
                            };
                            inst
                        }
                    });

                    current_block.instr.push(get_element_iref);
                } else {
                    let shift_iref = fun_ver.new_inst(Instruction {
                        hdr: MuEntityHeader::unnamed(vm.vm.next_id()),
                        value: Some(vec![iref_var.clone_value()]),
                        ops: {
                            let mut ops = vec![v.clone(), array_idx.clone()];
                            ops
                        },
                        v: {
                            let inst = Instruction_::ShiftIRef {
                                is_ptr: false,
                                base: 0,
                                offset: 1
                            };
                            inst
                        }
                    });

                    current_block.instr.push(shift_iref);
                }

                let result = fun_ver.new_ssa(
                    MuEntityHeader::named(
                        vm.vm.next_id(),
                        Arc::new(result_var_name)
                    ),
                    mu_var_type
                );

                vm.vm.set_name(result.as_entity());

                let load_instr = fun_ver.new_inst(Instruction {
                    hdr: MuEntityHeader::unnamed(vm.vm.next_id()),
                    value: Some(vec![result.clone_value()]),
                    ops: {
                        let mut ops = vec![iref_var.clone()];
                        ops
                    },
                    v: {
                        let inst = Instruction_::Load {
                            is_ptr: false,
                            order: MemoryOrder::SeqCst,
                            mem_loc: 0
                        };
                        inst
                    }
                });

                current_block.instr.push(load_instr);

                Ok(result.clone())
            }
            None => unimplemented!()
        }
    }

    fn get_type(
        &self,
        var_type_table: &HashMap<String, Type, RandomState>,
        fn_type_table: &HashMap<String, Type, RandomState>
    ) -> Type {
        match var_type_table.get(self.var.as_str()).unwrap() {
            Type::IntArray(_) => Type::Int,
            Type::DoubleArray(_) => Type::Double,
            Type::BoolArray(_) => Type::Bool,
            _ => Type::Void
        }
    }
}

impl Exp for ArrayLiteral {
    fn print(&self) -> String {
        let mut exps = vec![];

        for exp in self.exps() {
            exps.push(exp.print())
        }
        format!("ArrayExp({:?})", exps)
    }

    unsafe fn code_gen(
        &self,
        module: &*mut LLVMModule,
        context: &mut Context
    ) -> Result<*mut LLVMValue, String> {
        let mut llvm_exps = vec![];

        for exp in self.exps() {
            llvm_exps.push(exp.code_gen(module, context).unwrap());
        }

        let elem_type = self
            .exps()
            .get(0)
            .unwrap()
            .get_type(&context.var_type_map, &context.fn_type_map);

        let array_const = LLVMConstArray(
            context.get_llvm_type(&elem_type),
            llvm_exps.as_ptr() as *mut LLVMValueRef,
            self.exps.len() as u32
        );

        Ok(array_const)
    }

    unsafe fn code_gen_mu(
        &self,
        vm: &mut Mu_VM,
        fun_ver: &mut MuFunctionVersion
    ) -> Result<Arc<TreeNode>, String> {
        let name = vm.generate_name(format!("const_array"));

        let current_block = vm.blocks_map.get_mut(&vm.curr_block).unwrap();

        let mut mu_exps = vec![];

        let value_type =
            self.get_type(&current_block.var_type_map, &vm.fn_type_map);

        let mu_value_type = vm.get_mu_type(&value_type);

        for exp in self.exps() {
            let mu_exp = match exp.code_gen_mu(vm, fun_ver) {
                Ok(r) => r,
                e @ Err(_) => return e
            };

            mu_exps.push(mu_exp.as_value().clone());
        }

        let value = vm.vm.declare_const(
            MuEntityHeader::named(vm.vm.next_id(), Arc::new(name)),
            mu_value_type,
            Constant::List(mu_exps)
        );

        let value_local = fun_ver.new_constant(value.clone());

        Ok(value_local)
    }

    fn get_type(
        &self,
        var_type_table: &HashMap<String, Type, RandomState>,
        fn_type_table: &HashMap<String, Type, RandomState>
    ) -> Type {
        if self.exps.len() > 0 {
            let exp_type = self
                .exps()
                .get(0)
                .unwrap()
                .get_type(var_type_table, fn_type_table);
            for exp in self.exps() {
                assert_eq!(
                    exp_type,
                    exp.get_type(var_type_table, fn_type_table)
                )
            }

            match exp_type {
                Type::Int => Type::IntArray(self.exps.len() as i64),
                Type::Bool => Type::BoolArray(self.exps.len() as i64),
                Type::Double => Type::DoubleArray(self.exps.len() as i64),
                _ => panic!("Unsupported type.")
            }
        } else {
            panic!("Cannot get type of an empty array.")
        }
    }
}

impl Exp for VarExp {
    fn print(&self) -> String {
        let mut assigned_exps = vec![];
        for (var_type, var, exp) in &self.vars {
            assigned_exps.push(format!(
                "Assign({:?}, {}, {})",
                var_type,
                var.print(),
                exp.print()
            ));
        }
        format!("VarExp({:?}, {})", assigned_exps, self.body_exp.print())
    }

    unsafe fn code_gen(
        &self,
        module: &*mut LLVMModule,
        context: &mut Context
    ) -> Result<*mut LLVMValue, String> {
        let mut old_bindings = vec![];
        let mut old_type_bindings = vec![];

        let llvm_block = LLVMGetInsertBlock(context.builder);
        for var in &self.vars {
            let (var_type, name, init_expr) = var;

            let init_value: *mut LLVMValue =
                match init_expr.code_gen(module, context) {
                    Ok(result) => result,
                    e @ Err(_) => return e
                };

            // initializing type of arrays with its proper dimension
            let var_type = match var_type {
                Type::IntArray(-1) => init_expr
                    .get_type(&context.var_type_map, &context.fn_type_map),
                Type::BoolArray(-1) => init_expr
                    .get_type(&context.var_type_map, &context.fn_type_map),
                Type::DoubleArray(-1) => init_expr
                    .get_type(&context.var_type_map, &context.fn_type_map),
                t => t.clone()
            };

            old_type_bindings
                .push(context.var_type_map.remove(name.name.as_str()));
            context
                .var_type_map
                .insert(name.name.clone(), var_type.clone());
            let variable =
                create_block_alloca(context, llvm_block, name.name.as_str());

            LLVMBuildStore(context.builder, init_value, variable);
            old_bindings.push(context.named_values.remove(name.name.as_str()));

            context.named_values.insert(name.name.clone(), variable);
        }

        let body_value: *mut LLVMValue =
            match self.body_exp.code_gen(module, context) {
                Ok(result) => result,
                e @ Err(_) => return e
            };

        let mut old_bindings_iter = old_bindings.iter();
        let mut old_type_bindings_iter = old_type_bindings.iter();
        for var in &self.vars {
            let (var_type, name, _) = var;

            context.named_values.remove(name.name.as_str());
            context.var_type_map.remove(name.name.as_str());

            match old_bindings_iter.next() {
                Some(&Some(value)) => {
                    context.named_values.insert(name.name.clone(), value);
                    let old_type = old_type_bindings_iter
                        .next()
                        .unwrap()
                        .as_ref()
                        .unwrap();
                    context
                        .var_type_map
                        .insert(name.name.clone(), old_type.clone());
                }
                _ => ()
            }
        }

        Ok(body_value)
    }

    unsafe fn code_gen_mu(
        &self,
        vm: &mut Mu_VM,
        fun_ver: &mut MuFunctionVersion
    ) -> Result<Arc<TreeNode>, String> {
        let mut old_bindings = vec![];
        let mut old_var_type = vec![];
        for var in &self.vars {
            let (var_type, name, init_expr) = var;

            let mut start_exp = match init_expr.code_gen_mu(vm, fun_ver) {
                Ok(r) => r,
                e @ Err(_) => return e
            };

            match var_type {
                Type::IntArray(n)
                | Type::BoolArray(n)
                | Type::DoubleArray(n) => {
                    let current_block =
                        vm.blocks_map.get_mut(&vm.curr_block).unwrap();
                    let init_exp_type = init_expr
                        .get_type(&current_block.var_type_map, &vm.fn_type_map);
                    let mu_array_type = vm.get_mu_type(&init_exp_type);

                    let var = match init_exp_type {
                        Type::IntArray(n)
                        | Type::DoubleArray(n)
                        | Type::BoolArray(n) => {
                            start_exp = initialize_mu_array(
                                name.name(),
                                vm,
                                fun_ver,
                                &init_exp_type,
                                start_exp.clone()
                            );
                            let current_block =
                                vm.blocks_map.get_mut(&vm.curr_block).unwrap();
                            start_exp.clone()
                        }
                        _ => {
                            let var_name = vm.generate_name(name.name.clone());
                            let var = fun_ver.new_ssa(
                                MuEntityHeader::named(
                                    vm.vm.next_id(),
                                    Arc::new(var_name)
                                ),
                                vm.get_mu_type(&init_exp_type)
                            );

                            vm.vm.set_name(var.as_entity());

                            var
                        }
                    };

                    let current_block =
                        vm.blocks_map.get_mut(&vm.curr_block).unwrap();
                    current_block.env.push(var.clone_value());

                    old_bindings
                        .push(current_block.store.remove(name.name.as_str()));
                    old_var_type.push(
                        current_block.var_type_map.remove(name.name.as_str())
                    );
                    current_block.store.insert(name.name.clone(), start_exp);
                    current_block
                        .var_type_map
                        .insert(name.name.clone(), init_exp_type.clone());
                }
                _ => {
                    let var_name = vm.generate_name(name.name.clone());
                    let var = fun_ver.new_ssa(
                        MuEntityHeader::named(
                            vm.vm.next_id(),
                            Arc::new(var_name)
                        ),
                        vm.get_mu_type(var_type)
                    );

                    vm.vm.set_name(var.as_entity());

                    let current_block =
                        vm.blocks_map.get_mut(&vm.curr_block).unwrap();
                    current_block.env.push(var.clone_value());

                    old_bindings
                        .push(current_block.store.remove(name.name.as_str()));
                    old_var_type.push(
                        current_block.var_type_map.remove(name.name.as_str())
                    );
                    current_block.store.insert(name.name.clone(), start_exp);
                    current_block
                        .var_type_map
                        .insert(name.name.clone(), var_type.clone());
                }
            }
        }

        let body_value = match self.body_exp.code_gen_mu(vm, fun_ver) {
            Ok(r) => r,
            e @ Err(_) => return e
        };

        let mut old_bindings_iter = old_bindings.into_iter();
        let mut old_var_type_iter = old_var_type.into_iter();

        for var in &self.vars {
            let (var_type, name, _) = var;

            let current_block = vm.blocks_map.get_mut(&vm.curr_block).unwrap();
            current_block.store.remove(name.name.as_str());

            match old_bindings_iter.next() {
                Some(Some(value)) => {
                    current_block.store.insert(name.name.clone(), value);
                    let var_type = old_var_type_iter.next().unwrap().unwrap();
                    current_block
                        .var_type_map
                        .insert(name.name.clone(), var_type.clone());
                }
                _ => ()
            }
        }

        Ok(body_value)
    }

    fn get_type(
        &self,
        var_type_table: &HashMap<String, Type, RandomState>,
        fn_type_table: &HashMap<String, Type, RandomState>
    ) -> Type {
        let mut local_vars = var_type_table.clone();
        for (var_type, var, _) in self.vars() {
            local_vars.insert(var.name.clone(), var_type.clone());
        }

        self.body_exp.get_type(&local_vars, fn_type_table)
    }
}

fn initialize_mu_array(
    var_name: &String,
    vm: &mut Mu_VM,
    fun_ver: &mut MuFunctionVersion,
    exp_type: &Type,
    init_exp: P<TreeNode>
) -> P<TreeNode> {
    let (array_elem_type, i_ref_array_elem_type, i_ref_var_type, ref_var_type) =
        match exp_type {
            Type::IntArray(n) => (
                Type::Int,
                RefType::IRef(Type::Int),
                RefType::IRef(Type::IntArray(*n)),
                RefType::Ref(Type::IntArray(*n))
            ),
            Type::DoubleArray(n) => (
                Type::Double,
                RefType::IRef(Type::Double),
                RefType::IRef(Type::DoubleArray(*n)),
                RefType::Ref(Type::DoubleArray(*n))
            ),
            Type::BoolArray(n) => (
                Type::Bool,
                RefType::IRef(Type::Bool),
                RefType::IRef(Type::BoolArray(*n)),
                RefType::Ref(Type::BoolArray(*n))
            ),
            _ => panic!("Unsupported type.")
        };

    let var_name_ref = vm.generate_name(var_name.clone());
    let var = fun_ver.new_ssa(
        MuEntityHeader::named(vm.vm.next_id(), Arc::new(var_name_ref)),
        vm.get_mu_ref_type(&i_ref_var_type)
    );

    vm.vm.set_name(var.as_entity());

    let new_instr = fun_ver.new_inst(Instruction {
        hdr: MuEntityHeader::unnamed(vm.vm.next_id()),
        value: Some(vec![var.clone_value()]),
        ops: vec![],
        v: {
            let inst = Instruction_::AllocA(vm.get_mu_type(&exp_type));
            inst
        }
    });

    let current_block = vm.blocks_map.get_mut(&vm.curr_block).unwrap();
    current_block.instr.push(new_instr);

    let mut first_elem = var.clone();

    // for each array element get its elementiref and store the correspondent
    // value into it
    match &init_exp.as_value().v {
        Value_::Constant(Constant::List(values)) => {
            for idx in 0..values.len() {
                let temp_iref_name = format!("{}_elem_iref_{}", var_name, idx);
                let var_elem_name = vm.generate_name(temp_iref_name.clone());
                let ref_mu_array_elem_type =
                    vm.get_mu_ref_type(&i_ref_array_elem_type);
                let mu_array_elem_type = vm.get_mu_type(&array_elem_type);
                let var_elem = fun_ver.new_ssa(
                    MuEntityHeader::named(
                        vm.vm.next_id(),
                        Arc::new(var_elem_name)
                    ),
                    ref_mu_array_elem_type.clone()
                );

                vm.vm.set_name(var_elem.as_entity());

                if idx == 0 {
                    first_elem = var_elem.clone();
                }

                let value_const =
                    fun_ver.new_constant(values.get(idx).unwrap().clone());

                let idx_mu = vm.vm.declare_const(
                    MuEntityHeader::unnamed(vm.vm.next_id()),
                    mu_array_elem_type.clone(),
                    Constant::Int(idx as u64)
                );

                let idx_mu_local = fun_ver.new_constant(idx_mu.clone());

                let get_element_iref_instr = fun_ver.new_inst(Instruction {
                    hdr: MuEntityHeader::unnamed(vm.vm.next_id()),
                    value: Some(vec![var_elem.clone_value()]),
                    ops: {
                        let mut ops = vec![var.clone(), idx_mu_local.clone()];
                        ops
                    },
                    v: {
                        let inst = Instruction_::GetElementIRef {
                            is_ptr: false,
                            base: 0,
                            index: 1
                        };
                        inst
                    }
                });

                let store_instr = fun_ver.new_inst(Instruction {
                    hdr: MuEntityHeader::unnamed(vm.vm.next_id()),
                    value: None,
                    ops: {
                        let mut ops =
                            vec![var_elem.clone(), value_const.clone()];
                        ops
                    },
                    v: {
                        let inst = Instruction_::Store {
                            is_ptr: false,
                            order: MemoryOrder::SeqCst,
                            mem_loc: 0,
                            value: 1
                        };
                        inst
                    }
                });

                let current_block =
                    vm.blocks_map.get_mut(&vm.curr_block).unwrap();
                current_block.instr.push(get_element_iref_instr);
                current_block.instr.push(store_instr);
            }
        }
        _ => panic!("Unsupported array constant.")
    }

    var
}

impl Exp for ForExp {
    fn print(&self) -> String {
        format!(
            "ForExpr({:?}, {}, {}, {}, {}, {})",
            self.var_type,
            self.var.print(),
            self.start_exp.print(),
            self.end_exp.print(),
            self.step_exp.print(),
            self.body_exp.print(),
        )
    }

    unsafe fn code_gen(
        &self,
        module: &*mut LLVMModule,
        context: &mut Context
    ) -> Result<*mut LLVMValue, String> {
        context
            .var_type_map
            .insert(self.var.name.clone(), self.var_type.clone());

        let start_exp: *mut LLVMValue =
            match self.start_exp().code_gen(module, context) {
                Ok(result) => result,
                e @ Err(_) => return e
            };

        let preheader_block = LLVMGetInsertBlock(context.builder);
        let mut function = LLVMGetBasicBlockParent(preheader_block);
        let preloop_block = LLVMAppendBasicBlockInContext(
            context.context,
            function,
            "preloop\0".as_ptr() as *const c_char
        );

        let variable = create_block_alloca(
            context,
            preheader_block,
            self.var.name.clone().as_str()
        );
        LLVMBuildStore(context.builder, start_exp, variable);

        LLVMBuildBr(context.builder, preloop_block);
        LLVMPositionBuilderAtEnd(context.builder, preloop_block);

        let old_value = context.named_values.remove(self.var.name());
        context
            .named_values
            .insert(self.var.name().clone(), variable);

        let end_exp: *mut LLVMValue =
            match self.end_exp().code_gen(module, context) {
                Ok(result) => result,
                e @ Err(_) => return e
            };

        let zero_bool =
            LLVMConstInt(context.get_llvm_type(&Type::Bool), 0, true as i32);
        let zero_double =
            LLVMConstReal(context.get_llvm_type(&Type::Double), 0.0);
        let end_cond = LLVMBuildICmp(
            context.builder,
            LLVMIntNE,
            end_exp,
            zero_bool,
            "loopcond\0".as_ptr() as *const c_char
        );

        let mut after_block = LLVMAppendBasicBlockInContext(
            context.context,
            function,
            "afterloop\0".as_ptr() as *const c_char
        );
        let mut loop_block = LLVMAppendBasicBlockInContext(
            context.context,
            function,
            "loop\0".as_ptr() as *const c_char
        );

        LLVMBuildCondBr(context.builder, end_cond, loop_block, after_block);
        LLVMPositionBuilderAtEnd(context.builder, loop_block);

        match self.body_exp().code_gen(module, context) {
            Ok(result) => result,
            e @ Err(_) => return e
        };

        let step_exp: *mut LLVMValue =
            match self.step_exp().code_gen(module, context) {
                Ok(result) => result,
                e @ Err(_) => return e
            };

        let cur_value = LLVMBuildLoad(
            context.builder,
            variable,
            self.var.name.clone().as_str().as_ptr() as *const c_char
        );
        let next_value = LLVMBuildFAdd(
            context.builder,
            cur_value,
            step_exp,
            "nextvar\0".as_ptr() as *const c_char
        );
        LLVMBuildStore(context.builder, next_value, variable);

        let loop_end_block = LLVMGetInsertBlock(context.builder);

        LLVMBuildBr(context.builder, preloop_block);

        LLVMPositionBuilderAtEnd(context.builder, after_block);

        context.named_values.remove(self.var.name());
        match old_value {
            Some(v) => {
                context.named_values.insert(self.var.name().clone(), v);
            }
            None => ()
        }

        Ok(zero_double)
    }

    unsafe fn code_gen_mu(
        &self,
        vm: &mut Mu_VM,
        fun_ver: &mut MuFunctionVersion
    ) -> Result<Arc<TreeNode>, String> {
        let start_exp = match self.start_exp.code_gen_mu(vm, fun_ver) {
            Ok(r) => r,
            e @ Err(_) => return e
        };

        // create preloop block
        let preloop_block = Block::new(MuEntityHeader::named(
            vm.vm.next_id(),
            Arc::new(vm.generate_name(String::from("preloop")))
        ));
        &vm.vm.set_name(preloop_block.as_entity());

        let preloop_block_id = preloop_block.hdr.id();
        let phi_node_name = vm.generate_name(String::from("phi_node"));
        let phi_node = fun_ver.new_ssa(
            MuEntityHeader::named(
                vm.vm.next_id(),
                Arc::new(phi_node_name.clone())
            ),
            vm.get_mu_type(&self.var_type)
        );
        vm.vm.set_name(phi_node.as_entity());

        // branch to preloop
        let current_block = vm.blocks_map.get_mut(&vm.curr_block).unwrap();
        let current_block_env = (&current_block.env).clone();
        let current_block_form_args = (&current_block.store).clone();
        let current_block_var_type_map = (&current_block.var_type_map).clone();
        let mut args_preloop = vec![];
        let mut store_before_preloop = LinkedHashMap::new();
        let mut var_type_map_before_preloop = HashMap::new();
        let mut args_before_preloop = vec![];

        let mut preloop_store = LinkedHashMap::new();
        let mut preloop_var_type_map = HashMap::new();

        for (arg_name, value) in current_block_form_args.iter() {
            let arg_type = current_block_var_type_map.get(arg_name).unwrap();

            let form_arg = fun_ver.new_ssa(
                MuEntityHeader::named(
                    vm.vm.next_id(),
                    Arc::new(arg_name.clone())
                ),
                vm.get_mu_type(arg_type)
            );
            vm.vm.set_name(form_arg.as_entity());

            preloop_store.insert(arg_name.clone(), form_arg.clone());
            preloop_var_type_map.insert(arg_name.clone(), arg_type.clone());
            store_before_preloop.insert(arg_name.clone(), form_arg.clone());
            var_type_map_before_preloop
                .insert(arg_name.clone(), arg_type.clone());
            args_preloop.push(form_arg.clone_value());
            args_before_preloop.push(form_arg.clone_value());
        }

        let mut args = vec![];
        for (_, arg) in &current_block_form_args {
            args.push(arg.clone());
        }
        args.push(start_exp);

        let branch_preloop = create_branch1_instr(
            vm.vm.next_id(),
            fun_ver,
            args,
            &preloop_block
        );
        let current_block = vm.blocks_map.get_mut(&vm.curr_block).unwrap();
        current_block.instr.push(branch_preloop);

        preloop_store.insert(phi_node_name.clone(), phi_node.clone());
        preloop_var_type_map
            .insert(self.var.name.clone(), self.var_type.clone());
        args_preloop.push(phi_node.clone_value());

        vm.blocks_map.insert(
            preloop_block_id,
            Mu_Block {
                block: preloop_block.clone(),
                instr: vec![],
                block_actual_args: args_preloop.clone(),
                args_mapping: preloop_store.clone(),
                store: preloop_store.clone(),
                var_type_map: preloop_var_type_map.clone(),
                env: args_preloop.clone()
            }
        );

        vm.curr_block = preloop_block_id;
        let current_block = vm.blocks_map.get_mut(&vm.curr_block).unwrap();

        let old_value = current_block.store.remove(self.var.name());
        current_block
            .store
            .insert(self.var.name().clone(), phi_node.clone());

        // build end loop check
        let end_exp = match self.end_exp.code_gen_mu(vm, fun_ver) {
            Ok(r) => r,
            Err(err) => return Err(err)
        };

        let name = format!("const_{}", 0);
        let name = vm.generate_name(name);
        let const_type = vm.get_mu_type(&Type::Bool);
        let false_const = vm.vm.declare_const(
            MuEntityHeader::named(vm.vm.next_id(), Arc::new(name)),
            const_type,
            Constant::Int(1)
        );

        let false_local = fun_ver.new_constant(false_const.clone());

        let loop_check = fun_ver.new_ssa(
            MuEntityHeader::named(
                vm.vm.next_id(),
                Arc::new(vm.generate_name(String::from("loop_check")))
            ),
            vm.get_mu_type(&Type::Int)
        );

        vm.vm.set_name(loop_check.as_entity());

        let loop_check_inst = create_int_cmp_op_instruction_eq(
            fun_ver,
            vm,
            &loop_check,
            vec![end_exp.clone(), false_local.clone()]
        );
        let current_block = vm.blocks_map.get_mut(&vm.curr_block).unwrap();
        current_block.instr.push(loop_check_inst);

        // create after_loop and loop blocks
        let loop_block = Block::new(MuEntityHeader::named(
            vm.vm.next_id(),
            Arc::new(vm.generate_name(String::from("loop")))
        ));
        &vm.vm.set_name(loop_block.as_entity());

        let loop_block_id = loop_block.hdr.id();

        vm.blocks_map.insert(
            loop_block_id,
            Mu_Block {
                block: loop_block.clone(),
                instr: vec![],
                block_actual_args: args_preloop.clone(),
                args_mapping: preloop_store.clone(),
                store: preloop_store.clone(),
                var_type_map: preloop_var_type_map.clone(),
                env: args_preloop.clone()
            }
        );

        let after_loop_block = Block::new(MuEntityHeader::named(
            vm.vm.next_id(),
            Arc::new(vm.generate_name(String::from("after_loop")))
        ));
        &vm.vm.set_name(after_loop_block.as_entity());

        let after_loop_block_id = after_loop_block.hdr.id();

        vm.blocks_map.insert(
            after_loop_block_id,
            Mu_Block {
                block: after_loop_block.clone(),
                instr: vec![],
                block_actual_args: args_before_preloop.clone(),
                args_mapping: store_before_preloop.clone(),
                store: store_before_preloop.clone(),
                var_type_map: var_type_map_before_preloop.clone(),
                env: args_before_preloop.clone()
            }
        );

        let mut ops = vec![loop_check];
        for arg in preloop_store.values() {
            ops.push(arg.clone())
        }
        ops.push(phi_node.clone());

        let form_args_true = vm.blocks_map.get(&loop_block_id).unwrap();
        let form_args_false = vm.blocks_map.get(&after_loop_block_id).unwrap();

        let branch_loop_check = create_branch2_instr(
            vm.vm.next_id(),
            fun_ver,
            ops,
            form_args_true.store.clone(),
            form_args_false.store.clone(),
            &loop_block,
            &after_loop_block
        );

        let current_block = vm.blocks_map.get_mut(&vm.curr_block).unwrap();
        current_block.instr.push(branch_loop_check);

        vm.curr_block = loop_block_id;
        match self.body_exp.code_gen_mu(vm, fun_ver) {
            Ok(r) => r,
            e @ Err(_) => return e
        };

        let step_exp = match self.step_exp.code_gen_mu(vm, fun_ver) {
            Ok(r) => r,
            e @ Err(_) => return e
        };

        let name = vm.generate_name(String::from("step"));
        let var = fun_ver.new_ssa(
            MuEntityHeader::named(vm.vm.next_id(), Arc::new(name)),
            vm.get_mu_type(&self.var_type)
        );

        vm.vm.set_name(var.as_entity());

        let step_inst = fun_ver.new_inst(Instruction {
            hdr: MuEntityHeader::unnamed(vm.vm.next_id()),
            value: Some(vec![var.clone_value()]),
            ops: vec![phi_node.clone(), step_exp.clone()],
            v: Instruction_::BinOp(BinOp::FAdd, 0, 1)
        });

        let current_block = vm.blocks_map.get_mut(&loop_block_id).unwrap();
        let mut ops = vec![];
        for (name, arg) in store_before_preloop {
            let new_arg = current_block.store.get(name.as_str()).unwrap();
            ops.push(new_arg.clone())
        }
        ops.push(var.clone());

        let branch_preloop =
            create_branch1_instr(vm.vm.next_id(), fun_ver, ops, &preloop_block);

        let current_block = vm.blocks_map.get_mut(&vm.curr_block).unwrap();
        current_block.instr.push(step_inst);
        current_block.instr.push(branch_preloop);

        vm.curr_block = after_loop_block_id;
        let preloop_block = vm.blocks_map.get_mut(&preloop_block_id).unwrap();
        match old_value {
            Some(v) => {
                preloop_block.store.insert(self.var.name.clone(), v);
            }
            None => ()
        }

        // for loop returns 0.0
        let name = vm.generate_name(String::from("zero").clone());
        let zero_const_type = vm.get_mu_type(&Type::Double);

        let zero_const = vm.vm.declare_const(
            MuEntityHeader::named(vm.vm.next_id(), Arc::new(name)),
            zero_const_type,
            Constant::Double(0.0)
        );

        let zero_local = fun_ver.new_constant(zero_const.clone());

        Ok(zero_local)
    }

    fn get_type(
        &self,
        var_type_table: &HashMap<String, Type, RandomState>,
        fn_type_table: &HashMap<String, Type, RandomState>
    ) -> Type {
        Type::Void
    }
}

impl Exp for ConditionalExp {
    fn print(&self) -> String {
        format!(
            "CondExpr({:?}, {:?}, {:?})",
            self.cond.print(),
            self.then_branch.print(),
            self.else_branch.print()
        )
    }

    unsafe fn code_gen(
        &self,
        module: &*mut LLVMModule,
        context: &mut Context
    ) -> Result<*mut LLVMValue, String> {
        let cond: *mut LLVMValue = match self.cond().code_gen(module, context) {
            Ok(result) => result,
            e @ Err(_) => return e
        };

        let zero =
            LLVMConstInt(context.get_llvm_type(&Type::Bool), 0, true as i32);
        let ifcond = LLVMBuildICmp(
            context.builder,
            LLVMIntNE,
            cond,
            zero,
            "if_cond\0".as_ptr() as *const c_char
        );

        let block = LLVMGetInsertBlock(context.builder);
        let mut function = LLVMGetBasicBlockParent(block);
        let mut then_block = LLVMAppendBasicBlockInContext(
            context.context,
            function,
            "then\0".as_ptr() as *const c_char
        );
        let mut else_block = LLVMAppendBasicBlockInContext(
            context.context,
            function,
            "else\0".as_ptr() as *const c_char
        );
        let mut merge_block = LLVMAppendBasicBlockInContext(
            context.context,
            function,
            "ifcont\0".as_ptr() as *const c_char
        );

        let if_type =
            self.get_type(&context.var_type_map, &context.fn_type_map);

        LLVMBuildCondBr(context.builder, ifcond, then_block, else_block);

        LLVMPositionBuilderAtEnd(context.builder, then_block);
        let then_value: *mut LLVMValue =
            match self.then_branch().code_gen(module, context) {
                Ok(result) => result,
                e @ Err(_) => return e
            };
        LLVMBuildBr(context.builder, merge_block);
        let then_end_block = LLVMGetInsertBlock(context.builder);

        LLVMPositionBuilderAtEnd(context.builder, else_block);
        let else_value: *mut LLVMValue =
            match self.else_branch().code_gen(module, context) {
                Ok(result) => result,
                e @ Err(_) => return e
            };
        LLVMBuildBr(context.builder, merge_block);
        let else_end_block = LLVMGetInsertBlock(context.builder);

        LLVMPositionBuilderAtEnd(context.builder, merge_block);
        let mut phi = LLVMBuildPhi(
            context.builder,
            context.get_llvm_type(&if_type),
            "ifphi\0".as_ptr() as *const c_char
        );

        LLVMAddIncoming(
            phi,
            vec![then_value].as_mut_slice().as_mut_ptr(),
            vec![then_end_block].as_mut_slice().as_mut_ptr(),
            1 as c_uint
        );
        LLVMAddIncoming(
            phi,
            vec![else_value].as_mut_slice().as_mut_ptr(),
            vec![else_end_block].as_mut_slice().as_mut_ptr(),
            1 as c_uint
        );

        Ok(phi)
    }

    unsafe fn code_gen_mu(
        &self,
        vm: &mut Mu_VM,
        fun_ver: &mut MuFunctionVersion
    ) -> Result<Arc<TreeNode>, String> {
        let current_block = vm.blocks_map.get_mut(&vm.curr_block).unwrap();
        let if_type =
            self.get_type(&current_block.var_type_map, &vm.fn_type_map);

        // process and create cond
        let cond = match self.cond.code_gen_mu(vm, fun_ver) {
            Ok(r) => r,
            e @ Err(_) => return e
        };

        let name = format!("const_{}", 0);
        let name = vm.generate_name(name);
        let true_const_type = vm.get_mu_type(&Type::Bool);

        let true_const = vm.vm.declare_const(
            MuEntityHeader::named(vm.vm.next_id(), Arc::new(name)),
            true_const_type,
            Constant::Int(1)
        );

        let true_local = fun_ver.new_constant(true_const.clone());

        let var = fun_ver.new_ssa(
            MuEntityHeader::named(
                vm.vm.next_id(),
                Arc::new(vm.generate_name(String::from("if_cond")))
            ),
            vm.get_mu_type(&Type::Bool)
        );

        vm.vm.set_name(var.as_entity());

        let ifcond_inst = create_int_cmp_op_instruction_eq(
            fun_ver,
            vm,
            &var,
            vec![cond.clone(), true_local.clone()]
        );

        // create blocks
        let then_branch_block = Block::new(MuEntityHeader::named(
            vm.vm.next_id(),
            Arc::new(vm.generate_name(String::from("then_branch")))
        ));
        &vm.vm.set_name(then_branch_block.as_entity());

        let then_branch_block_id = then_branch_block.hdr.id();

        let else_branch_block = Block::new(MuEntityHeader::named(
            vm.vm.next_id(),
            Arc::new(vm.generate_name(String::from("else_branch")))
        ));
        &vm.vm.set_name(else_branch_block.as_entity());

        let else_branch_block_id = else_branch_block.hdr.id();

        let if_cont_branch_block = Block::new(MuEntityHeader::named(
            vm.vm.next_id(),
            Arc::new(vm.generate_name(String::from("if_cont")))
        ));
        &vm.vm.set_name(if_cont_branch_block.as_entity());

        let if_cont_branch_block_id = if_cont_branch_block.hdr.id();

        let current_block = vm.blocks_map.get_mut(&vm.curr_block).unwrap();
        current_block.instr.push(ifcond_inst.clone());

        // create branch instr
        let current_block_args = (&current_block.block_actual_args).clone();
        let current_block_form_args = (&current_block.args_mapping).clone();
        let current_block_type_map = (&current_block.var_type_map).clone();

        let mut ops = vec![var.clone()];
        for arg in current_block_form_args.values() {
            ops.push(arg.clone())
        }

        let branch_then_inst = create_branch2_instr(
            vm.vm.next_id(),
            fun_ver,
            ops,
            current_block_form_args.clone(),
            current_block_form_args.clone(),
            &then_branch_block,
            &else_branch_block
        );

        current_block.instr.push(branch_then_inst.clone());

        vm.blocks_map.insert(
            then_branch_block_id,
            Mu_Block {
                block: then_branch_block,
                instr: vec![],
                block_actual_args: current_block_args.clone(),
                args_mapping: current_block_form_args.clone(),
                store: current_block_form_args.clone(),
                var_type_map: current_block_type_map.clone(),
                env: current_block_args.clone()
            }
        );

        // process then branch
        vm.curr_block = then_branch_block_id;

        let then_branch_expr = match self.then_branch.code_gen_mu(vm, fun_ver) {
            Ok(r) => r,
            e @ Err(_) => return e
        };

        let mut args = vec![];
        for (_, arg) in &current_block_form_args {
            args.push(arg.clone());
        }
        args.push(then_branch_expr);

        let branch_if_cont_inst = create_branch1_instr(
            vm.vm.next_id(),
            fun_ver,
            args,
            &if_cont_branch_block
        );
        let current_block = vm.blocks_map.get_mut(&vm.curr_block).unwrap();
        current_block.instr.push(branch_if_cont_inst);

        vm.blocks_map.insert(
            else_branch_block.hdr.id(),
            Mu_Block {
                block: else_branch_block,
                instr: vec![],
                block_actual_args: current_block_args.clone(),
                args_mapping: current_block_form_args.clone(),
                store: current_block_form_args.clone(),
                var_type_map: current_block_type_map.clone(),
                env: current_block_args.clone()
            }
        );

        // process else branch
        vm.curr_block = else_branch_block_id;

        let else_branch_expr = match self.else_branch.code_gen_mu(vm, fun_ver) {
            Ok(r) => r,
            e @ Err(_) => return e
        };

        let mut args = vec![];
        for (_, arg) in &current_block_form_args {
            args.push(arg.clone());
        }
        args.push(else_branch_expr);

        let branch_if_cont_inst = create_branch1_instr(
            vm.vm.next_id(),
            fun_ver,
            args,
            &if_cont_branch_block
        );

        let current_block = vm.blocks_map.get_mut(&vm.curr_block).unwrap();
        current_block.instr.push(branch_if_cont_inst);

        // process if_cont block
        let mut args_if_cont = current_block.env.clone();
        let mut form_args_if_cont = current_block.store.clone();
        let mut var_type_map_if_cont = current_block.var_type_map.clone();

        let phi_node_name = vm.generate_name(String::from("phi_node"));
        let phi_node = fun_ver.new_ssa(
            MuEntityHeader::named(
                vm.vm.next_id(),
                Arc::new(phi_node_name.clone())
            ),
            vm.get_mu_type(&if_type)
        );

        vm.vm.set_name(phi_node.as_entity());
        form_args_if_cont.insert(phi_node_name.clone(), phi_node.clone());
        var_type_map_if_cont.insert(phi_node_name.clone(), if_type.clone());
        args_if_cont.push(phi_node.clone_value());

        vm.blocks_map.insert(
            if_cont_branch_block.hdr.id(),
            Mu_Block {
                block: if_cont_branch_block,
                instr: vec![],
                block_actual_args: args_if_cont.clone(),
                args_mapping: form_args_if_cont.clone(),
                store: form_args_if_cont.clone(),
                var_type_map: var_type_map_if_cont.clone(),
                env: args_if_cont.clone()
            }
        );

        vm.curr_block = if_cont_branch_block_id;
        Ok(phi_node)
    }

    fn get_type(
        &self,
        var_type_table: &HashMap<String, Type, RandomState>,
        fn_type_table: &HashMap<String, Type, RandomState>
    ) -> Type {
        let type_then =
            self.then_branch.get_type(var_type_table, fn_type_table);
        let type_else =
            self.else_branch.get_type(var_type_table, fn_type_table);

        assert_eq!(type_then, type_else);
        assert_eq!(
            self.cond.get_type(var_type_table, fn_type_table),
            Type::Bool
        );

        type_then.clone()
    }
}

fn create_branch2_instr(
    id: MuID,
    fun_ver: &mut MuFunctionVersion,
    ops: Vec<P<TreeNode>>,
    form_args_true: LinkedHashMap<String, P<TreeNode>>,
    form_args_false: LinkedHashMap<String, P<TreeNode>>,
    true_dest: &Block,
    false_dest: &Block
) -> P<TreeNode> {
    fun_ver.new_inst(Instruction {
        hdr: MuEntityHeader::unnamed(id),
        value: None,
        ops,
        v: Instruction_::Branch2 {
            cond: 0,
            true_dest: Destination {
                target: true_dest.hdr.clone(),
                args: {
                    let mut args = vec![];
                    for pos_arg in 1..form_args_true.len() + 1 {
                        args.push(pos_arg)
                    }
                    args.iter()
                        .map(|x| -> DestArg { DestArg::Normal(*x) })
                        .collect()
                }
            },
            false_dest: Destination {
                target: false_dest.hdr.clone(),
                args: {
                    let mut args = vec![];
                    for pos_arg in 1..form_args_false.len() + 1 {
                        args.push(pos_arg)
                    }
                    args.iter()
                        .map(|x| -> DestArg { DestArg::Normal(*x) })
                        .collect()
                }
            },
            true_prob: 0.5
        }
    })
}

fn create_branch1_instr(
    id: MuID,
    fun_ver: &mut MuFunctionVersion,
    form_args: Vec<P<TreeNode>>,
    dest: &Block
) -> P<TreeNode> {
    fun_ver.new_inst(Instruction {
        hdr: MuEntityHeader::unnamed(id),
        value: None,
        ops: {
            let mut ops = vec![];
            for arg in &form_args {
                ops.push(arg.clone())
            }
            ops
        },
        v: {
            let inst = Instruction_::Branch1(Destination {
                target: dest.hdr.clone(),
                args: {
                    let mut args = vec![];
                    for pos_arg in 0..form_args.len() {
                        args.push(pos_arg)
                    }
                    args.iter()
                        .map(|x| -> DestArg { DestArg::Normal(*x) })
                        .collect()
                }
            });
            inst
        }
    })
}

impl Exp for BinaryExp {
    fn print(&self) -> String {
        format!(
            "BinaryExp({}, {:?}, {})",
            self.lhs.print(),
            self.c,
            self.rhs.print()
        )
    }

    unsafe fn code_gen(
        &self,
        module: &*mut LLVMModule,
        context: &mut Context
    ) -> Result<*mut LLVMValue, String> {
        let lhs: *mut LLVMValue = match self.lhs().code_gen(module, context) {
            Ok(result) => result,
            e @ Err(_) => return e
        };

        let rhs: *mut LLVMValue = match self.rhs().code_gen(module, context) {
            Ok(result) => result,
            e @ Err(_) => return e
        };

        let exp_type =
            self.get_type(&context.var_type_map, &context.fn_type_map);

        match self.c() {
            '+' => match exp_type {
                Type::Double => Ok(LLVMBuildFAdd(
                    context.builder,
                    lhs,
                    rhs,
                    "addtmp\0".as_ptr() as *const c_char
                )),
                Type::Int => Ok(LLVMBuildAdd(
                    context.builder,
                    lhs,
                    rhs,
                    "addtmp\0".as_ptr() as *const c_char
                )),
                _ => Err("Unsupported type".parse().unwrap())
            },
            '-' => match exp_type {
                Type::Double => Ok(LLVMBuildFSub(
                    context.builder,
                    lhs,
                    rhs,
                    "subtmp\0".as_ptr() as *const c_char
                )),
                Type::Int => Ok(LLVMBuildSub(
                    context.builder,
                    lhs,
                    rhs,
                    "subtmp\0".as_ptr() as *const c_char
                )),
                _ => Err("Unsupported type".parse().unwrap())
            },
            '*' => match exp_type {
                Type::Double => Ok(LLVMBuildFMul(
                    context.builder,
                    lhs,
                    rhs,
                    "multmp\0".as_ptr() as *const c_char
                )),
                Type::Int => Ok(LLVMBuildMul(
                    context.builder,
                    lhs,
                    rhs,
                    "multmp\0".as_ptr() as *const c_char
                )),
                _ => Err("Unsupported type".parse().unwrap())
            },
            '<' => {
                let lhs_type = self
                    .lhs
                    .get_type(&context.var_type_map, &context.fn_type_map);

                match lhs_type {
                    Type::Double => {
                        let cmp = LLVMBuildFCmp(
                            context.builder,
                            LLVMRealOLT,
                            lhs,
                            rhs,
                            "cmptmp\0".as_ptr() as *const c_char
                        );

                        Ok(cmp)
                    }
                    Type::Int => {
                        let cmp = LLVMBuildICmp(
                            context.builder,
                            LLVMIntULT,
                            lhs,
                            rhs,
                            "cmptmp\0".as_ptr() as *const c_char
                        );

                        Ok(cmp)
                    }
                    _ => Err("Unsupported type".parse().unwrap())
                }
            }
            '=' => {
                let var_name = self.lhs().print(); // format!("{}", self.lhs().print());

                match context.global_vars.get(var_name.as_str()) {
                    Some((var_type, value)) => {
                        context
                            .global_vars
                            .insert(var_name.clone(), (var_type.clone(), rhs));
                        return Ok(rhs);
                    }
                    None => ()
                }

                let variable = match context.named_values.get(var_name.as_str())
                {
                    Some(vl) => *vl,
                    None => return Err(String::from("Unknown variable name"))
                };

                LLVMBuildStore(context.builder, rhs, variable);

                Ok(rhs)
            }
            _ => Err(String::from("Invalid binary operator."))
        }
    }

    unsafe fn code_gen_mu(
        &self,
        mu_vm: &mut Mu_VM,
        function_version: &mut MuFunctionVersion
    ) -> Result<P<TreeNode>, String> {
        let lhs = match self.lhs.code_gen_mu(mu_vm, function_version) {
            Ok(r) => r,
            e @ Err(_) => return e
        };

        let rhs = match self.rhs.code_gen_mu(mu_vm, function_version) {
            Ok(r) => r,
            Err(err) => return Err(err)
        };

        let current_block =
            mu_vm.blocks_map.get_mut(&mu_vm.curr_block).unwrap();
        let exp_type =
            self.get_type(&current_block.var_type_map, &mu_vm.fn_type_map);

        let var = function_version.new_ssa(
            MuEntityHeader::named(
                mu_vm.vm.next_id(),
                Arc::new(String::from("r"))
            ),
            mu_vm.get_mu_type(&exp_type)
        );

        mu_vm.vm.set_name(var.as_entity());

        let op = match self.c() {
            '+' => match exp_type {
                Type::Double => BinOp::FAdd,
                Type::Int => BinOp::Add,
                _ => return Err("Unsupported type".parse().unwrap())
            },
            '-' => match exp_type {
                Type::Double => BinOp::FSub,
                Type::Int => BinOp::Sub,
                _ => return Err("Unsupported type".parse().unwrap())
            },
            '*' => match exp_type {
                Type::Double => BinOp::FMul,
                Type::Int => BinOp::Mul,
                _ => return Err("Unsupported type".parse().unwrap())
            },
            '<' => {
                let var = function_version.new_ssa(
                    MuEntityHeader::named(
                        mu_vm.vm.next_id(),
                        Arc::new(String::from("r"))
                    ),
                    mu_vm.get_mu_type(&Type::Bool)
                );

                let current_block =
                    mu_vm.blocks_map.get_mut(&mu_vm.curr_block).unwrap();
                let lhs_type = self
                    .lhs
                    .get_type(&current_block.var_type_map, &mu_vm.fn_type_map);
                let op = match lhs_type {
                    Type::Double => CmpOp::FOLT,
                    Type::Int => CmpOp::SLT,
                    _ => return Err("Unsupported type".parse().unwrap())
                };

                mu_vm.vm.set_name(var.as_entity());

                let cmpop = create_cmp_op_instruction(
                    function_version,
                    mu_vm,
                    &var,
                    vec![lhs.clone(), rhs.clone()],
                    op
                );

                let current_block =
                    mu_vm.blocks_map.get_mut(&mu_vm.curr_block).unwrap();
                current_block.instr.push(cmpop);

                return Ok(var);
            }
            '=' => {
                let var_name = self.lhs().print();

                match mu_vm.global_vars.get(var_name.as_str()) {
                    Some((var_type, value)) => {
                        mu_vm.global_vars.insert(
                            var_name.clone(),
                            (var_type.clone(), rhs.clone())
                        );

                        return Ok(rhs);
                    }
                    None => ()
                };

                let current_block =
                    mu_vm.blocks_map.get_mut(&mu_vm.curr_block).unwrap();
                current_block.store.insert(var_name.clone(), rhs.clone());
                return Ok(rhs);
            }
            _ => unimplemented!()
        };

        let binop = function_version.new_inst(Instruction {
            hdr: MuEntityHeader::unnamed(mu_vm.vm.next_id()),
            value: Some(vec![var.clone_value()]),
            ops: vec![lhs.clone(), rhs.clone()],
            v: Instruction_::BinOp(op, 0, 1)
        });

        let current_block =
            mu_vm.blocks_map.get_mut(&mu_vm.curr_block).unwrap();
        current_block.instr.push(binop);

        Ok(var)
    }

    fn get_type(
        &self,
        var_type_table: &HashMap<String, Type, RandomState>,
        fn_type_table: &HashMap<String, Type, RandomState>
    ) -> Type {
        if self.c == '<' {
            return Type::Bool;
        }

        if self.c == '=' {
            return self.rhs.get_type(var_type_table, fn_type_table);
        }

        let type_lhs = self.lhs.get_type(var_type_table, fn_type_table);
        let type_rhs = self.rhs.get_type(var_type_table, fn_type_table);

        assert_eq!(type_lhs, type_rhs);
        type_lhs.clone()
    }
}

impl Exp for RealLiteral {
    fn print(&self) -> String {
        format!("Real({})", self.val)
    }

    unsafe fn code_gen(
        &self,
        module: &*mut LLVMModule,
        context: &mut Context
    ) -> Result<LLVMValueRef, String> {
        Ok(LLVMConstReal(
            context.get_llvm_type(&Type::Double),
            self.val()
        ))
    }

    unsafe fn code_gen_mu(
        &self,
        vm: &mut Mu_VM,
        function_version: &mut MuFunctionVersion
    ) -> Result<P<TreeNode>, String> {
        let name = format!("const_{}", self.val);
        let value_type = vm.get_mu_type(&Type::Double);

        let value = vm.vm.declare_const(
            MuEntityHeader::named(vm.vm.next_id(), Arc::new(name)),
            value_type,
            Constant::Double(self.val)
        );

        let value_local = function_version.new_constant(value.clone());

        Ok(value_local)
    }

    fn get_type(
        &self,
        var_type_table: &HashMap<String, Type, RandomState>,
        fn_type_table: &HashMap<String, Type, RandomState>
    ) -> Type {
        Type::Double
    }
}

impl Exp for IntLiteral {
    fn print(&self) -> String {
        format!("Int({})", self.val)
    }

    unsafe fn code_gen(
        &self,
        module: &*mut LLVMModule,
        context: &mut Context
    ) -> Result<LLVMValueRef, String> {
        Ok(LLVMConstInt(
            context.get_llvm_type(&Type::Int),
            self.val as u64,
            false as LLVMBool
        ))
    }

    unsafe fn code_gen_mu(
        &self,
        vm: &mut Mu_VM,
        function_version: &mut MuFunctionVersion
    ) -> Result<P<TreeNode>, String> {
        let name = format!("const_{}", self.val);
        let value_type = vm.get_mu_type(&Type::Int);

        let value = vm.vm.declare_const(
            MuEntityHeader::named(vm.vm.next_id(), Arc::new(name)),
            value_type,
            Constant::Int(self.val as u64)
        );

        let value_local = function_version.new_constant(value.clone());

        Ok(value_local)
    }

    fn get_type(
        &self,
        var_type_table: &HashMap<String, Type, RandomState>,
        fn_type_table: &HashMap<String, Type, RandomState>
    ) -> Type {
        Type::Int
    }
}

impl Exp for BoolLiteral {
    fn print(&self) -> String {
        format!("Bool({})", self.val)
    }

    unsafe fn code_gen(
        &self,
        module: &*mut LLVMModule,
        context: &mut Context
    ) -> Result<LLVMValueRef, String> {
        let val = if self.val { 1 } else { 0 };
        Ok(LLVMConstInt(
            context.get_llvm_type(&Type::Bool),
            val,
            false as i32
        ))
    }

    unsafe fn code_gen_mu(
        &self,
        vm: &mut Mu_VM,
        function_version: &mut MuFunctionVersion
    ) -> Result<P<TreeNode>, String> {
        let val = if self.val { 1 } else { 0 };
        let name = format!("const_{}", self.val);
        let value_type = vm.get_mu_type(&Type::Bool);

        let value = vm.vm.declare_const(
            MuEntityHeader::named(vm.vm.next_id(), Arc::new(name)),
            value_type,
            Constant::Int(val)
        );

        let value_local = function_version.new_constant(value.clone());

        Ok(value_local)
    }

    fn get_type(
        &self,
        var_type_table: &HashMap<String, Type, RandomState>,
        fn_type_table: &HashMap<String, Type, RandomState>
    ) -> Type {
        Type::Bool
    }
}

impl Exp for Var {
    fn print(&self) -> String {
        self.name.clone()
    }

    unsafe fn code_gen(
        &self,
        module: &*mut LLVMModule,
        context: &mut Context
    ) -> Result<*mut LLVMValue, String> {
        let global_var = return_var_value(&self, module, context, true);

        match global_var {
            t @ Ok(_) => return t,
            Err(e) => ()
        };

        let local_var = return_var_value(&self, module, context, false);

        match local_var {
            t @ Ok(_) => return t,
            Err(e) => panic!("Variable not found.")
        }
    }

    unsafe fn code_gen_mu(
        &self,
        mu_vm: &mut Mu_VM,
        function_version: &mut MuFunctionVersion
    ) -> Result<P<TreeNode>, String> {
        match mu_vm.global_vars.get(self.name.as_str()) {
            Some((var_type, val)) => return Ok(val.clone()),
            None => ()
        }

        let current_block =
            mu_vm.blocks_map.get_mut(&mu_vm.curr_block).unwrap();
        let var_type =
            self.get_type(&current_block.var_type_map, &mu_vm.fn_type_map);

        match var_type {
            Type::IntArray(_) => {
                let iref_var_name = format!("{}_iref", self.name);
                let iref_var_name = mu_vm.generate_name(iref_var_name);
                let iref_var = function_version.new_ssa(
                    MuEntityHeader::named(
                        mu_vm.vm.next_id(),
                        Arc::new(iref_var_name)
                    ),
                    mu_vm.get_mu_ref_type(&RefType::IRef(Type::Int))
                );

                mu_vm.vm.set_name(iref_var.as_entity());

                let current_block =
                    mu_vm.blocks_map.get_mut(&mu_vm.curr_block).unwrap();
                let var = current_block.store.get(self.name.as_str()).unwrap();

                let get_iref_instr = function_version.new_inst(Instruction {
                    hdr: MuEntityHeader::unnamed(mu_vm.vm.next_id()),
                    value: Some(vec![iref_var.clone_value()]),
                    ops: vec![var.clone()],
                    v: {
                        let inst = Instruction_::GetIRef(0);
                        inst
                    }
                });

                let current_block =
                    mu_vm.blocks_map.get_mut(&mu_vm.curr_block).unwrap();
                current_block.instr.push(get_iref_instr);
                return Ok(iref_var);
            }
            Type::BoolArray(_) => {}
            Type::DoubleArray(_) => {}
            _ => ()
        }

        let r = current_block.store.get(self.name.as_str());
        match r {
            Some(v) => Ok(v.clone()),
            None => panic!("Variable not found.")
        }
    }

    fn get_type(
        &self,
        var_type_table: &HashMap<String, Type, RandomState>,
        fn_type_table: &HashMap<String, Type, RandomState>
    ) -> Type {
        var_type_table.get(self.name.as_str()).unwrap().clone()
    }
}

pub unsafe fn return_var_value(
    var: &Var,
    module: &*mut LLVMModule,
    context: &mut Context,
    global: bool
) -> Result<*mut LLVMValue, String> {
    if global == false {
        match context.named_values.get(var.name()) {
            Some(val) => {
                let name = format!("{}\0", var.name.clone());
                match var.get_type(&context.var_type_map, &context.fn_type_map)
                {
                    Type::Int | Type::Bool | Type::Double => {
                        let var = LLVMBuildLoad(
                            context.builder,
                            *val,
                            name.as_str().as_ptr() as *const c_char
                        );
                        Ok(var)
                    }
                    Type::IntArray(_)
                    | Type::BoolArray(_)
                    | Type::DoubleArray(_) => {
                        let pos = [
                            LLVMConstInt(
                                context.get_llvm_type(&Type::Int),
                                0,
                                false as i32
                            ),
                            LLVMConstInt(
                                context.get_llvm_type(&Type::Int),
                                0,
                                false as i32
                            )
                        ];

                        let pointer = LLVMBuildInBoundsGEP(
                            context.builder,
                            *val,
                            pos.as_ptr() as *mut LLVMValueRef,
                            2,
                            "tmp\0".as_ptr() as *const c_char
                        );

                        Ok(pointer)
                    }
                    _ => panic!("Unsupported type")
                }
            }
            None => Err(String::from("Variable not found."))
        }
    } else {
        match context.global_vars.get(var.name()) {
            Some((var_type, val)) => {
                let name = format!("{}\0", var.name.clone());
                match var_type {
                    Type::Int | Type::Bool | Type::Double => Ok(*val),
                    Type::IntArray(_)
                    | Type::BoolArray(_)
                    | Type::DoubleArray(_) => {
                        let pos = [
                            LLVMConstInt(
                                context.get_llvm_type(&Type::Int),
                                0,
                                false as i32
                            ),
                            LLVMConstInt(
                                context.get_llvm_type(&Type::Int),
                                0,
                                false as i32
                            )
                        ];

                        let pointer = LLVMBuildInBoundsGEP(
                            context.builder,
                            *val,
                            pos.as_ptr() as *mut LLVMValueRef,
                            2,
                            "tmp\0".as_ptr() as *const c_char
                        );

                        Ok(pointer)
                    }
                    _ => panic!("Unsupported type")
                }
            }
            None => Err(String::from("Variable not found."))
        }
    }
}

impl Exp for CallExp {
    fn print(&self) -> String {
        let mut args = vec![];
        for arg in &self.args {
            args.push(arg.print());
        }

        format!("CallExp({}, {:?})", self.callee, args)
    }

    unsafe fn code_gen(
        &self,
        module: &*mut LLVMModule,
        context: &mut Context
    ) -> Result<*mut LLVMValue, String> {
        let function_name = CString::new(self.callee()).unwrap();
        let function = LLVMGetNamedFunction(
            *module,
            function_name.as_ptr() as *const c_char
        );

        if function.is_null() {
            return Err(String::from("Unknown function reference."));
        }

        if LLVMCountParams(function) != self.args().len() as u32 {
            return Err(String::from("Incorrect number of parameters"));
        }

        let mut args_value = vec![];
        for arg in self.args().iter() {
            let arg: *mut LLVMValue = match arg.code_gen(module, context) {
                Ok(result) => result,
                e @ Err(_) => return e
            };
            args_value.push(arg);
        }

        Ok(LLVMBuildCall(
            context.builder,
            function,
            args_value.as_mut_ptr(),
            args_value.len() as c_uint,
            "calltmp\0".as_ptr() as *const c_char
        ))
    }

    unsafe fn code_gen_mu(
        &self,
        mu_vm: &mut Mu_VM,
        function_version: &mut MuFunctionVersion
    ) -> Result<P<TreeNode>, String> {
        let function_args: Vec<Result<P<TreeNode>, String>> = self
            .args
            .iter()
            .map(|e| (**e).code_gen_mu(mu_vm, function_version))
            .collect();

        let callee = match mu_vm.functions.get(self.callee.as_str()) {
            Some(t) => t,
            None => match mu_vm.extern_prototypes.get(self.callee.as_str()) {
                Some(proto) => {
                    return code_gen_extern_call(
                        self.callee(),
                        proto.clone(),
                        function_args,
                        mu_vm,
                        function_version
                    );
                }
                None => {
                    return Err(String::from("Function has not been defined."))
                }
            }
        };

        let callee_type =
            mu_vm.fn_type_map.get(self.callee.as_str()).unwrap().clone();

        let mut args = vec![callee.clone()];

        for arg in function_args {
            match arg {
                Ok(t) => args.push(t),
                e @ Err(_) => return e
            }
        }

        let args_len = args.len();
        let call_result_type = mu_vm.get_mu_type(&callee_type);

        let call_result = function_version.new_ssa(
            MuEntityHeader::named(
                mu_vm.vm.next_id(),
                Arc::new(String::from("c"))
            ),
            call_result_type
        );

        mu_vm.vm.set_name(call_result.as_entity());

        let expr_call = function_version.new_inst(Instruction {
            hdr: MuEntityHeader::unnamed(mu_vm.vm.next_id()),
            value: Some(vec![call_result.clone_value()]),
            ops: args,
            v: Instruction_::ExprCall {
                data: CallData {
                    func: 0,
                    args: (1..args_len).collect(),
                    convention: CallConvention::Mu
                },
                is_abort: false
            }
        });

        let current_block =
            mu_vm.blocks_map.get_mut(&mu_vm.curr_block).unwrap();
        current_block.instr.push(expr_call);

        Ok(call_result)
    }

    fn get_type(
        &self,
        var_type_table: &HashMap<String, Type, RandomState>,
        fn_type_table: &HashMap<String, Type, RandomState>
    ) -> Type {
        fn_type_table.get(self.callee.as_str()).unwrap().clone()
    }
}

fn code_gen_extern_call(
    name: String,
    proto: Prototype,
    f_args: Vec<Result<P<TreeNode>, String>>,
    mu_vm: &mut Mu_VM,
    function_version: &mut MuFunctionVersion
) -> Result<P<TreeNode>, String> {
    let args_type: Vec<P<MuType>> = proto
        .args
        .iter()
        .map(|(arg_type, arg_name)| -> P<MuType> {
            mu_vm.get_mu_type(arg_type)
        })
        .collect();

    let return_type = mu_vm.get_mu_type(proto.return_type());

    let ext_func_sig = mu_vm.vm.declare_func_sig(
        MuEntityHeader::named(
            mu_vm.vm.next_id(),
            Arc::new(format!("{}_sig", name))
        ),
        vec![return_type],
        args_type
    );
    mu_vm.vm.set_name(ext_func_sig.as_entity());

    typedef!((mu_vm.vm) ufp_extern = mu_ufuncptr(ext_func_sig));

    //    constdef!((mu_vm.vm) <ufp_extern> const_extern = Constant::ExternSym(C
    // (name.as_str())));

    let const_extern = mu_vm.vm.declare_const(
        MuEntityHeader::named(
            mu_vm.vm.next_id(),
            Arc::new(format!("{}_const", name))
        ),
        ufp_extern.clone(),
        Constant::ExternSym(Arc::new(name.clone()))
    );

    mu_vm.vm.set_name(const_extern.as_entity());

    consta!((mu_vm.vm, function_version) const_extern_local = const_extern);

    let mut args = vec![const_extern_local.clone()];

    for arg in f_args {
        match arg {
            Ok(t) => args.push(t),
            e @ Err(_) => return e
        }
    }

    //    inst!((vm, func_ver) ret:
    //        EXPRCCALL (CallConvention::Foreign(ForeignFFI::C), is_abort:
    // false) const_exit_local (arg)    );

    let call_result = function_version.new_ssa(
        MuEntityHeader::named(mu_vm.vm.next_id(), Arc::new(String::from("ec"))),
        mu_vm.get_mu_type(proto.return_type())
    );

    mu_vm.vm.set_name(call_result.as_entity());

    let args_len = args.len();
    let ccall_inst = function_version.new_inst(Instruction {
        hdr: MuEntityHeader::unnamed(mu_vm.vm.next_id()),
        value: Some(vec![call_result.clone_value()]),
        ops: args,
        v: Instruction_::ExprCCall {
            data: CallData {
                func: 0,
                args: (1..args_len).collect(),
                convention: CallConvention::Foreign(ForeignFFI::C)
            },
            is_abort: false
        }
    });

    let current_block = mu_vm.blocks_map.get_mut(&mu_vm.curr_block).unwrap();
    current_block.instr.push(ccall_inst);

    Ok(call_result)
}
