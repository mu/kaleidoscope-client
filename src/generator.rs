use crate::ast::*;
use crate::parser::*;
use llvm::prelude::LLVMValueRef;
use std::collections::HashMap;
use std::ffi::{CStr, CString};
use std::{iter, ptr};

extern crate llvm_sys as llvm;

use self::llvm::analysis::LLVMVerifierFailureAction::{
    LLVMAbortProcessAction, LLVMPrintMessageAction
};
use self::llvm::analysis::{LLVMVerifyFunction, LLVMVerifyModule};
use self::llvm::execution_engine::{
    LLVMCreateExecutionEngineForModule, LLVMCreateJITCompilerForModule,
    LLVMCreateSimpleMCJITMemoryManager, LLVMDisposeExecutionEngine,
    LLVMExecutionEngineRef, LLVMFreeMachineCodeForFunction,
    LLVMGenericValueToFloat, LLVMGenericValueToInt, LLVMGenericValueToPointer,
    LLVMInitializeMCJITCompilerOptions, LLVMLinkInMCJIT,
    LLVMMCJITCompilerOptions, LLVMOpaqueExecutionEngine, LLVMRunFunction
};
use self::llvm::prelude::{
    LLVMBasicBlockRef, LLVMBool, LLVMBuilderRef, LLVMContextRef,
    LLVMPassManagerRef, LLVMTypeRef
};
use self::llvm::support::LLVMAddSymbol;
use self::llvm::target::{
    LLVMIntPtrType, LLVMTargetDataRef, LLVM_InitializeNativeAsmPrinter,
    LLVM_InitializeNativeTarget
};
use self::llvm::target_machine::{
    LLVMCodeGenFileType, LLVMCodeGenOptLevel, LLVMCodeModel,
    LLVMCreateTargetDataLayout, LLVMCreateTargetMachine,
    LLVMGetDefaultTargetTriple, LLVMGetFirstTarget, LLVMRelocMode,
    LLVMTargetMachineEmitToFile, LLVMTargetMachineRef
};
use self::llvm::transforms::scalar::{
    LLVMAddBasicAliasAnalysisPass, LLVMAddCFGSimplificationPass,
    LLVMAddGVNPass, LLVMAddInstructionCombiningPass, LLVMAddReassociatePass
};
use self::llvm::transforms::util::LLVMAddPromoteMemoryToRegisterPass;
use self::llvm::LLVMRealPredicate::LLVMRealOLT;
use self::llvm::{LLVMModule, LLVMType, LLVMValue};
use crate::ast::Type::Annon;
use crate::lexer::Token::CHAR;
use llvm::core::*;
use std::borrow::Borrow;
use std::os::raw::{c_char, c_int, c_uint, c_void};

#[derive(Debug, PartialEq)]
pub enum RunResult {
    Double(f64),
    Int(i64),
    Bool(bool),
    IntArray(Vec<i64>)
}

pub struct Context {
    pub context: LLVMContextRef,
    pub builder: LLVMBuilderRef,
    pub named_values: HashMap<String, LLVMValueRef>,
    pub global_vars: HashMap<String, (Type, LLVMValueRef)>,
    pub var_type_map: HashMap<String, Type>,
    pub fn_type_map: HashMap<String, Type>,
    pub curr_function: i32,
    pub pass_manager: LLVMPassManagerRef,
    pub target_machine: LLVMTargetMachineRef,
    pub target_data: LLVMTargetDataRef,
    pub options: LLVMMCJITCompilerOptions
}

impl Context {
    pub unsafe fn new(pass_manager: LLVMPassManagerRef) -> Context {
        let context = llvm::core::LLVMContextCreate();
        let builder = llvm::core::LLVMCreateBuilderInContext(context);
        let named_values = HashMap::new();
        let mut options = std::mem::uninitialized();

        LLVMInitializeMCJITCompilerOptions(
            &mut options,
            std::mem::size_of::<LLVMMCJITCompilerOptions>()
        );

        LLVMLinkInMCJIT();
        LLVM_InitializeNativeTarget();
        LLVM_InitializeNativeAsmPrinter();
        LLVM_InitializeNativeTarget();

        let triple = LLVMGetDefaultTargetTriple();
        let target = LLVMGetFirstTarget();
        let cpu = CString::new("x86-64").expect("invalid cpu");
        let feature = CString::new("").expect("invalid feature");
        let opt_level = LLVMCodeGenOptLevel::LLVMCodeGenLevelNone;
        let reloc_mode = LLVMRelocMode::LLVMRelocDefault;
        let code_model = LLVMCodeModel::LLVMCodeModelDefault;
        let target_machine = LLVMCreateTargetMachine(
            target,
            triple,
            cpu.as_ptr(),
            feature.as_ptr(),
            opt_level,
            reloc_mode,
            code_model
        );
        let target_data = LLVMCreateTargetDataLayout(target_machine);

        Context {
            context,
            builder,
            named_values,
            global_vars: HashMap::new(),
            var_type_map: HashMap::new(),
            fn_type_map: HashMap::new(),
            curr_function: 0,
            pass_manager,
            target_machine,
            target_data,
            options
        }
    }

    pub unsafe fn dump(&self) {
        LLVMContextDispose(self.context);
    }

    pub unsafe fn get_llvm_type(&self, t: &Type) -> LLVMTypeRef {
        match t {
            Type::Bool => LLVMInt1Type(),
            Type::Int => LLVMInt64Type(),
            Type::Double => LLVMDoubleType(),
            Type::IntArray(-1) => LLVMPointerType(LLVMInt64Type(), 0),
            Type::IntArray(n) => LLVMArrayType(LLVMInt64Type(), *n as u32),
            Type::DoubleArray(n) => LLVMArrayType(LLVMDoubleType(), *n as u32),
            Type::BoolArray(n) => LLVMArrayType(LLVMInt1Type(), *n as u32),
            _ => panic!("Unsupported type.")
        }
    }
}

unsafe fn run_function(
    function: Result<*mut LLVMValue, String>,
    function_type: &Type,
    module: *mut LLVMModule,
    context: &mut Context
) -> Result<RunResult, String> {
    let mut ee = 0 as LLVMExecutionEngineRef;
    let mut error = 0 as *mut c_char;

    if LLVMVerifyModule(module, LLVMAbortProcessAction, &mut error) > 0 {
        let cstr_buf = std::ffi::CStr::from_ptr(error);
        let result = String::from_utf8_lossy(cstr_buf.to_bytes()).into_owned();
        let message = LLVMCreateMessage(error);
        let message = CString::from_raw(message);
        let message = format!("{:?}", message);
        return Err(message);
    }

    if LLVMCreateJITCompilerForModule(
        &mut ee,
        module,
        context.options.OptLevel,
        &mut error
    ) > 0
    {
        let cstr_buf = std::ffi::CStr::from_ptr(error);
        let result = String::from_utf8_lossy(cstr_buf.to_bytes()).into_owned();
        let message = LLVMCreateMessage(error);
        let message = CString::from_raw(message);
        let message = format!("{:?}", message);
        return Err(message);
    }

    let mut args = vec![];

    let (f, result) = match function {
        Ok(f) => (
            f,
            LLVMRunFunction(ee, f, args.len() as c_uint, args.as_mut_ptr())
        ),
        Err(e) => return Err(e)
    };

    let file_type = LLVMCodeGenFileType::LLVMAssemblyFile;
    let output_file = CString::new("output.S").expect("invalid file");

    let mut error_str = ptr::null_mut();
    let res = LLVMTargetMachineEmitToFile(
        context.target_machine,
        module,
        output_file.as_ptr() as *mut i8,
        file_type,
        &mut error_str
    );
    if res == 1 {
        let x = CStr::from_ptr(error_str);
        let result = String::from_utf8_lossy(x.to_bytes()).into_owned();
        let message = LLVMCreateMessage(error_str);
        let message = CString::from_raw(message);
        let message = format!("{:?}", message);
        return Err(message);
    }

    match function_type {
        Type::Double => {
            let result = LLVMGenericValueToFloat(LLVMDoubleType(), result);
            Ok(RunResult::Double(result))
        }
        Type::Int => {
            let result = LLVMGenericValueToInt(result, 1) as i64;
            Ok(RunResult::Int(result))
        }
        Type::Bool => {
            let result = LLVMGenericValueToInt(result, 1) != 0;
            Ok(RunResult::Bool(result))
        }
        //        Type::IntArray => {
        //            let result = LLVMGenericValueToPointer(result) as
        // Vec<i64>;            Ok(RunResult::IntArray(result))
        //        }
        _ => Err("invalid function type".parse().unwrap())
    }
}

pub extern "C" fn putchard(x: f64) -> f64 {
    print!("{}", x as u8 as char);
    x
}

pub unsafe fn init() {
    LLVMAddSymbol(
        "putchard\0".as_ptr() as *const c_char,
        putchard as *mut c_void
    );
}

pub unsafe fn code_gen(
    filename: String,
    mut nodes: ASTNodes
) -> Vec<Result<RunResult, String>> {
    init();
    let module = llvm::core::LLVMModuleCreateWithName(
        filename.as_str().as_ptr() as *const _
    );
    let function_pass_manager = LLVMCreateFunctionPassManagerForModule(module);
    LLVMAddBasicAliasAnalysisPass(function_pass_manager);
    LLVMAddInstructionCombiningPass(function_pass_manager);
    LLVMAddReassociatePass(function_pass_manager);
    LLVMAddGVNPass(function_pass_manager);
    LLVMAddCFGSimplificationPass(function_pass_manager);
    LLVMInitializeFunctionPassManager(function_pass_manager);
    LLVMAddPromoteMemoryToRegisterPass(function_pass_manager);

    let mut context = Context::new(function_pass_manager);

    let mut module_number = 0;
    let mut returning_values = vec![];

    for global_var in nodes.global_vars.iter_mut() {
        let out_file =
            CString::new(format!("out_{}.ll", module_number)).unwrap();
        global_var.code_gen(&module, &mut context);

        llvm::core::LLVMPrintModuleToFile(
            module,
            out_file.as_ptr(),
            ptr::null_mut()
        );
    }

    for prototype in nodes.prototypes.iter_mut() {
        let out_file =
            CString::new(format!("out_{}.ll", module_number)).unwrap();
        prototype.code_gen(&module, &mut context);

        llvm::core::LLVMPrintModuleToFile(
            module,
            out_file.as_ptr(),
            ptr::null_mut()
        );
    }

    for function in nodes.functions.iter_mut() {
        let out_file =
            CString::new(format!("out_{}.ll", module_number)).unwrap();

        let result = function.code_gen(&module, &mut context);

        llvm::core::LLVMPrintModuleToFile(
            module,
            out_file.as_ptr(),
            ptr::null_mut()
        );

        // execute module
        if function.proto().name() == "" {
            returning_values.push(run_function(
                result,
                function.proto().return_type(),
                module,
                &mut context
            ));
        }
        let module = llvm::core::LLVMModuleCreateWithName(
            filename.as_str().as_ptr() as *const _
        );
        let function_pass_manager =
            LLVMCreateFunctionPassManagerForModule(module);
        let context = Context::new(function_pass_manager);
    }

    returning_values
}

trait Code_gen {
    unsafe fn code_gen(
        &mut self,
        module: &*mut LLVMModule,
        context: &mut Context
    ) -> Result<LLVMValueRef, String>;
}

impl Code_gen for Global {
    unsafe fn code_gen(
        &mut self,
        module: &*mut LLVMModule,
        context: &mut Context
    ) -> Result<*mut LLVMValue, String> {
        let llvm_var_type = context.get_llvm_type(self.var_type());
        let var_name = self.var().name().clone();

        let global = LLVMAddGlobal(
            *module,
            llvm_var_type,
            var_name.as_str().as_ptr() as *const c_char
        );

        let init_value = match self.value().code_gen(module, context) {
            Ok(init_value) => init_value,
            e @ Err(_) => return e
        };

        context
            .global_vars
            .insert(var_name.clone(), (self.var_type().clone(), init_value));

        Ok(global)
    }
}

impl Code_gen for Prototype {
    unsafe fn code_gen(
        &mut self,
        module: &*mut LLVMModule,
        context: &mut Context
    ) -> Result<LLVMValueRef, String> {
        let function = match get_function_by_name(*module, self.name().as_str())
        {
            Some(function) => {
                if LLVMCountParams(function) as usize != self.args().len() {
                    return Err(String::from(
                        "Redefinition of function with different number of arguments.",
                    ));
                }

                if LLVMCountBasicBlocks(function) > 0 {
                    return Err(String::from("Redefinition of function"));
                }

                function
            }
            None => {
                let mut param_types = vec![];

                for (param_type, param_name) in self.args() {
                    param_types.push(context.get_llvm_type(param_type));
                    context
                        .var_type_map
                        .insert(param_name.clone(), param_type.clone());
                }

                let fty = LLVMFunctionType(
                    context.get_llvm_type(self.return_type()),
                    param_types.as_mut_ptr(),
                    param_types.len() as c_uint,
                    false as LLVMBool
                );
                let name;
                let fname;
                if self.name().as_str().eq("") {
                    fname = format!("f_anonn_{}", context.curr_function);

                    context.curr_function += 1;
                    name = CString::new(fname.as_str()).unwrap();
                } else {
                    fname = self.name().clone();
                    name = CString::new(self.name().clone().as_str()).unwrap();
                }

                let ftype = self.return_type();
                context.fn_type_map.insert(fname.clone(), ftype.clone());

                let function = LLVMAddFunction(
                    *module,
                    name.as_ptr() as *const c_char,
                    fty
                );

                for i in 0..self.args().len() {
                    let param = LLVMGetParam(function, i as u32);
                    let (param_type, param_name) = self.args().get(i).unwrap();

                    LLVMSetValueName2(
                        param,
                        param_name.as_str().as_ptr() as *const c_char,
                        param_name.len()
                    )
                }
                function
            }
        };

        let basic_blocs_count = LLVMCountBasicBlocks(function);

        Ok(function)
    }
}

pub fn get_function_by_name(
    function: *mut LLVMModule,
    name: &str
) -> Option<*mut LLVMValue> {
    let name = CString::new(name).unwrap();
    let function = unsafe {
        LLVMGetNamedFunction(function, name.as_ptr() as *const c_char)
    };
    if !function.is_null() {
        unsafe { Some(function) }
    } else {
        None
    }
}

pub unsafe fn create_block_alloca(
    context: &mut Context,
    block: LLVMBasicBlockRef,
    var_name: &str
) -> LLVMValueRef {
    let builder = LLVMCreateBuilder();
    let fi = LLVMGetFirstInstruction(block);
    LLVMPositionBuilder(builder, block, fi);
    let var = format!("{}\0", var_name);
    LLVMBuildAlloca(
        builder,
        context.get_llvm_type(context.var_type_map.get(var_name).unwrap()),
        var.as_str().as_ptr() as *const c_char
    )
}

impl Code_gen for Function {
    unsafe fn code_gen(
        &mut self,
        module: &*mut LLVMModule,
        context: &mut Context
    ) -> Result<LLVMValueRef, String> {
        context.named_values.clear();
        context.var_type_map.clear();

        if self.proto().return_type().eq(&Type::Annon) {
            let fn_type = self
                .body()
                .get_type(&context.var_type_map, &context.fn_type_map);
            self.proto().replace_annon_type(fn_type);
        }

        let function = match self.proto().code_gen(module, context) {
            Ok(function) => function,
            e @ Err(_) => return e
        };

        let mut basic_block = LLVMAppendBasicBlockInContext(
            context.context,
            function,
            CString::new("entry").unwrap().as_ptr()
        );

        LLVMPositionBuilderAtEnd(context.builder, basic_block);

        // set function parameters
        for (param, (arg_type, arg_name)) in
            get_params(function).iter().zip(self.proto().args())
        {
            let arg_alloca = create_block_alloca(
                context,
                basic_block,
                arg_name.clone().as_str()
            );
            LLVMBuildStore(context.builder, *param, arg_alloca);
            context.named_values.insert(arg_name.clone(), arg_alloca);
        }

        let body = match self.body().code_gen(module, context) {
            Ok(value) => value,
            e @ Err(_) => {
                LLVMDeleteFunction(function);
                return e;
            }
        };

        LLVMBuildRet(context.builder, body);

        //        LLVMVerifyFunction(function, LLVMAbortProcessAction);
        //        LLVMRunFunctionPassManager(context.pass_manager, function);

        Ok(function)
    }
}

unsafe fn get_params(function: *mut LLVMValue) -> Vec<LLVMValueRef> {
    let params_count = LLVMCountParams(function);
    let mut buf: Vec<LLVMValueRef> = Vec::with_capacity(params_count as usize);
    let p = buf.as_mut_ptr();
    unsafe {
        std::mem::forget(buf);
        LLVMGetParams(function, p);
        Vec::from_raw_parts(p, params_count as usize, params_count as usize)
    }
}
