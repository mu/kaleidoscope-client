use std::env::var;
use std::ops::Add;
use std::{fs, io};

#[derive(Debug)]
pub enum Token {
    EOF,
    DEF,
    EXTERN,
    ID(String),
    REAL_LIT(f64),
    INT_LIT(i64),
    BOOL_LIT(bool),
    CHAR(char),
    IF,
    THEN,
    ELSE,
    FOR,
    IN,
    VAR,
    BOOL,
    INT,
    DOUBLE,
    BOOL_ARRAY,
    INT_ARRAY,
    DOUBLE_ARRAY,
    GLOBAL
}

pub fn read_file() -> (String, Vec<Token>) {
    loop {
        println!("Enter filename or type '.quit' to close: ");

        let mut filename = String::new();

        let exit_code = String::from(".quit");

        if filename.trim().eq(&exit_code) {
            return (String::new(), vec![]);
        }

        let mut filename = String::from("test.txt");

        if !filename.ends_with(".txt") {
            filename = String::from(filename.trim());
            filename = filename.add(".txt");
        }

        match fs::read_to_string(filename.trim()) {
            Ok(contents) => {
                return (
                    String::from(filename.trim_end_matches(".txt")),
                    tokenize(contents)
                );
            }
            Err(e) => {
                println!("{}", e);
                continue;
            }
        };
    }
}

pub fn tokenize(input: String) -> Vec<Token> {
    let mut result = vec![];

    let mut index: i32 = 0;
    let input_str = input.as_bytes();

    while index < input.len() as i32 {
        let c = input_str[index as usize] as char;
        index = index + 1;

        if c.is_ascii_whitespace() {
            continue;
        }

        // parse keywords or identifiers
        if c.is_alphabetic() {
            result.push(parse_identifier_or_keyword(c, input_str, &mut index));
            continue;
        }

        // recognize numbers
        if c.is_numeric() || c == '.' {
            result.push(parse_number(c, input_str, &mut index));
            continue;
        }

        // recognize comments
        if c == '#' {
            parse_comments(input_str, &mut index);
            continue;
        }

        // recognized character
        result.push(Token::CHAR(c));
    }

    result.push(Token::EOF);
    result
}

fn parse_identifier_or_keyword(
    c: char,
    input_str: &[u8],
    index: &mut i32
) -> Token {
    let mut result = String::new();
    result.push(c);

    let mut openedBracket = false;

    while *index < input_str.len() as i32 {
        let c = input_str[*index as usize] as char;
        *index = *index + 1;

        if c.is_alphanumeric() & !openedBracket {
            result.push(c);
        } else if c.is_alphanumeric() & openedBracket {
            result.pop();
            *index = *index - 2;
            break;
        } else if c == '[' {
            result.push(c);
            openedBracket = true;
        } else if (c == ']') & openedBracket {
            result.push(c);
            break;
        } else if openedBracket {
            result.pop();
            result.pop();
            *index = *index - 2;
            break;
        } else {
            *index = *index - 1;
            break;
        }
    }

    let def_token = String::from("def");
    let extern_token = String::from("extern");
    let if_token = String::from("if");
    let then_token = String::from("then");
    let else_token = String::from("else");
    let for_token = String::from("for");
    let in_token = String::from("in");
    let var_token = String::from("var");
    let int_token = String::from("int");
    let bool_token = String::from("bool");
    let double_token = String::from("double");
    let int_array_token = String::from("int[]");
    let bool_array_token = String::from("bool[]");
    let double_array_token = String::from("double[]");
    let false_token = String::from("false");
    let true_token = String::from("true");
    let global_token = String::from("global");

    if result.eq(&def_token) {
        return Token::DEF;
    }

    if result.eq(&global_token) {
        return Token::GLOBAL;
    }

    if result.eq(&for_token) {
        return Token::FOR;
    }

    if result.eq(&in_token) {
        return Token::IN;
    }

    if result.eq(&extern_token) {
        return Token::EXTERN;
    }

    if result.eq(&if_token) {
        return Token::IF;
    }

    if result.eq(&then_token) {
        return Token::THEN;
    }

    if result.eq(&else_token) {
        return Token::ELSE;
    }

    if result.eq(&var_token) {
        return Token::VAR;
    }

    if result.eq(&int_token) {
        return Token::INT;
    }

    if result.eq(&bool_token) {
        return Token::BOOL;
    }

    if result.eq(&double_token) {
        return Token::DOUBLE;
    }

    if result.eq(&int_array_token) {
        return Token::INT_ARRAY;
    }

    if result.eq(&bool_array_token) {
        return Token::BOOL_ARRAY;
    }

    if result.eq(&double_array_token) {
        return Token::DOUBLE_ARRAY;
    }

    if result.eq(&false_token) {
        return Token::BOOL_LIT(false);
    }

    if result.eq(&true_token) {
        return Token::BOOL_LIT(true);
    }

    Token::ID(result)
}

fn parse_number(c: char, string_as_bytes: &[u8], index: &mut i32) -> Token {
    let mut result = String::new();
    result.push(c);
    let mut is_double = false;

    while *index < string_as_bytes.len() as i32 {
        let c = string_as_bytes[*index as usize] as char;
        *index = *index + 1;

        if c.is_numeric() || c == '.' {
            result.push(c);
            if c == '.' {
                is_double = true;
            }
        } else {
            *index = *index - 1;
            break;
        }
    }

    if is_double {
        let value = result.parse::<f64>().expect("Invalid number.");
        Token::REAL_LIT(value)
    } else {
        let value = result.parse::<i64>().expect("Invalid number.");
        Token::INT_LIT(value)
    }
}
//
fn parse_comments(string_as_bytes: &[u8], pos: &mut i32) {
    while *pos < string_as_bytes.len() as i32 {
        let c = string_as_bytes[*pos as usize] as char;
        *pos = *pos + 1;

        if c == '\n' || c == '\r' {
            break;
        }
    }
}
