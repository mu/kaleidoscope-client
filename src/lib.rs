#![allow(warnings)]
extern crate mu;
use self::mu::ast::types::*;

#[macro_use]
pub mod ir_macros;

pub mod ast;
pub mod generator;
pub mod lexer;
pub mod parser;

pub mod mu_generator;
pub mod tests;
