#![allow(warnings)]

extern crate mu;
use self::mu::ast::types::*;

#[macro_use]
pub mod ir_macros;

pub mod ast;
pub mod generator;
pub mod lexer;
pub mod parser;

pub mod mu_generator;

fn main() {
    const USE_LLVM: bool = false;
    const USE_AOT_MU_LLVM: bool = false;

    // process chars into tokens
    let (filename, tokens) = lexer::read_file();

    if !tokens.is_empty() {
        println!("Tokens: {:?}", tokens);
    }
    // parse tokens into tree
    let nodes = parser::parse_program(tokens);

    let values;
    if USE_LLVM {
        unsafe {
            values = generator::code_gen(filename, nodes);
        }
    } else {
        unsafe {
            values =
                mu_generator::code_gen(filename, nodes, USE_AOT_MU_LLVM, false);
        }
    }

    for value in values {
        match value {
            Ok(v) => println!("{:?}", v),
            Err(e) => println!("{}", e)
        }
    }
}
