extern crate llvm_sys as llvm;

use crate::ast::*;
use crate::generator::RunResult;
use crate::parser::*;
use mu::vm::api;
use mu::vm::api::api_c::*;
use std::os::raw::{c_char, c_void};
use std::ptr::null;

extern crate mu;
use mu::ast::inst::*;
use mu::ast::ir::*;
use mu::ast::op::*;
use mu::ast::types::*;
use mu::linkutils::*;
use mu::runtime::*;
use mu::utils::Address;
use mu::utils::LinkedHashMap;
use mu::vm::*;

extern crate mu_aot_llvm;

extern crate libloading as ll;

use std::sync::Arc;
pub type P<T> = Arc<T>;

#[macro_use]
use crate::ir_macros;
use self::mu::linkutils;
use mu::runtime::thread::MuThread;
use std::collections::{HashMap, HashSet};
use std::ops::Add;
use std::path::PathBuf;

pub struct Mu_VM {
    pub vm: Arc<VM>,
    pub curr_function: i32,
    pub curr_block: MuID,
    pub blocks_map: LinkedHashMap<MuID, Mu_Block>,
    pub global_vars: HashMap<String, (Type, P<TreeNode>)>,
    pub types_mu_types_map: HashMap<Type, P<MuType>>,
    pub ref_types_mu_types_map: HashMap<RefType, P<MuType>>,
    pub functions: HashMap<String, P<TreeNode>>,
    pub extern_prototypes: HashMap<String, Prototype>,
    pub whitelist: Vec<MuID>,
    pub last_runnable_function: Option<MuEntityHeader>,
    pub name_generator: HashMap<String, i32>,
    pub fn_type_map: HashMap<String, Type>,
    pub var_type_map: HashMap<String, Type>
}

impl Mu_VM {
    pub fn new(use_llvm_aot: bool, debug: bool) -> Mu_VM {
        let debug_opt = "--log-level=debug --emit-debug-info ";
        let generate_llvm_opt = "--generate-llvm";
        let mut opts = String::from("init_mu");

        if use_llvm_aot {
            opts = format!("{} {}", opts, generate_llvm_opt)
        };

        if debug {
            opts = format!("{} {}", opts, debug_opt)
        };

        let vm = Arc::new(VM::new_with_opts(opts.as_str()));

        let curr_function = 0;

        let mut types_mu_types_map = HashMap::new();
        let mut ref_types_mu_types_map = HashMap::new();

        let functions = HashMap::new();
        let extern_prototypes = HashMap::new();
        let whitelist = vec![];
        let mut blocks_map = LinkedHashMap::new();

        Mu_VM {
            vm,
            curr_function,
            curr_block: 0,
            blocks_map,
            global_vars: HashMap::new(),
            types_mu_types_map,
            ref_types_mu_types_map,
            functions,
            extern_prototypes,
            whitelist,
            last_runnable_function: None,
            name_generator: HashMap::new(),
            fn_type_map: HashMap::new(),
            var_type_map: HashMap::new()
        }
    }

    pub fn get_mu_ref_type(&mut self, t: &RefType) -> P<MuType> {
        if self.ref_types_mu_types_map.contains_key(t) {
            self.ref_types_mu_types_map.get(t).unwrap().clone()
        } else {
            match t {
                RefType::Ref(t) => {
                    let mu_type = self.get_mu_type(t);
                    let name = self.generate_name(String::from("ref_type"));
                    let mu_type_ref = self.vm.declare_type(
                        MuEntityHeader::named(
                            self.vm.next_id(),
                            Arc::new(name)
                        ),
                        MuType_::muref(mu_type)
                    );
                    self.vm.set_name(mu_type_ref.as_entity());

                    self.ref_types_mu_types_map
                        .insert(RefType::Ref(t.clone()), mu_type_ref.clone());
                    mu_type_ref
                }
                RefType::IRef(t) => {
                    let mu_type = self.get_mu_type(t);
                    let name = self.generate_name(String::from("i_ref_type"));
                    let mu_type_i_ref = self.vm.declare_type(
                        MuEntityHeader::named(
                            self.vm.next_id(),
                            Arc::new(name)
                        ),
                        MuType_::IRef(mu_type)
                    );
                    self.vm.set_name(mu_type_i_ref.as_entity());

                    self.ref_types_mu_types_map.insert(
                        RefType::IRef(t.clone()),
                        mu_type_i_ref.clone()
                    );
                    mu_type_i_ref
                }
            }
        }
    }

    pub fn get_mu_type(&mut self, t: &Type) -> P<MuType> {
        if self.types_mu_types_map.contains_key(t) {
            self.types_mu_types_map.get(t).unwrap().clone()
        } else {
            match t {
                Type::Double => {
                    let double_ty = self.vm.declare_type(
                        MuEntityHeader::named(
                            self.vm.next_id(),
                            Mu(stringify!(double))
                        ),
                        MuType_::double()
                    );
                    self.vm.set_name(double_ty.as_entity());

                    self.types_mu_types_map
                        .insert(Type::Double, double_ty.clone());
                    double_ty
                }
                Type::Int => {
                    let int_64_ty = self.vm.declare_type(
                        MuEntityHeader::named(
                            self.vm.next_id(),
                            Mu(stringify!(int_64))
                        ),
                        MuType_::int(64)
                    );
                    self.vm.set_name(int_64_ty.as_entity());

                    self.types_mu_types_map
                        .insert(Type::Int, int_64_ty.clone());
                    int_64_ty
                }
                Type::Bool => {
                    let int_1_ty = self.vm.declare_type(
                        MuEntityHeader::named(
                            self.vm.next_id(),
                            Mu(stringify!(int_1))
                        ),
                        MuType_::int(1)
                    );
                    self.vm.set_name(int_1_ty.as_entity());

                    self.types_mu_types_map
                        .insert(Type::Bool, int_1_ty.clone());
                    int_1_ty
                }
                Type::IntArray(n) => {
                    let type_var_name = format!("int_64_array_{}_ty", n);
                    let int_type = self.get_mu_type(&Type::Int);

                    let int_64_array_ty = self.vm.declare_type(
                        MuEntityHeader::named(
                            self.vm.next_id(),
                            Arc::new(type_var_name)
                        ),
                        MuType_::Array(int_type, *n as usize)
                    );
                    self.vm.set_name(int_64_array_ty.as_entity());

                    self.types_mu_types_map
                        .insert(Type::IntArray(*n), int_64_array_ty.clone());
                    int_64_array_ty
                }
                Type::BoolArray(n) => {
                    let type_var_name = format!("int_1_array_{}_ty", n);
                    let bool_type = self.get_mu_type(&Type::Bool);

                    let int_1_array_ty = self.vm.declare_type(
                        MuEntityHeader::named(
                            self.vm.next_id(),
                            Arc::new(type_var_name)
                        ),
                        MuType_::Array(bool_type, *n as usize)
                    );
                    self.vm.set_name(int_1_array_ty.as_entity());

                    self.types_mu_types_map
                        .insert(Type::BoolArray(*n), int_1_array_ty.clone());
                    int_1_array_ty
                }
                Type::DoubleArray(n) => {
                    let type_var_name = format!("double_array_{}_ty", n);
                    let double_type = self.get_mu_type(&Type::Double);

                    let double_array_ty = self.vm.declare_type(
                        MuEntityHeader::named(
                            self.vm.next_id(),
                            Arc::new(type_var_name)
                        ),
                        MuType_::Array(double_type, *n as usize)
                    );
                    self.vm.set_name(double_array_ty.as_entity());

                    self.types_mu_types_map
                        .insert(Type::BoolArray(*n), double_array_ty.clone());
                    double_array_ty
                }
                _ => panic!("Unsupported type.")
            }
        }
    }

    pub fn generate_name(&mut self, name: String) -> String {
        match self.name_generator.get(name.as_str()) {
            Some(idx) => {
                let result = format!("{}_{}", name, idx);
                self.name_generator.insert(name, idx + 1);
                result
            }
            None => {
                let result = format!("{}", name);
                self.name_generator.insert(name, 1);
                result
            }
        }
    }
}

pub struct Mu_Block {
    pub block: Block,
    pub instr: Vec<P<TreeNode>>,
    pub block_actual_args: Vec<P<Value>>,
    pub args_mapping: LinkedHashMap<String, P<TreeNode>>,
    pub store: LinkedHashMap<String, P<TreeNode>>,
    pub var_type_map: HashMap<String, Type>,
    pub env: Vec<P<Value>>
}

pub unsafe fn code_gen(
    filename: String,
    mut nodes: ASTNodes,
    use_llvm_aot: bool,
    debug: bool
) -> Vec<Result<RunResult, String>> {
    let mut vm = Mu_VM::new(use_llvm_aot, debug);
    let mut results = vec![];

    for global in nodes.global_vars {
        let var_name = global.var().name();
        let var_type = global.var_type().clone();
        let func_ver: &mut MuFunctionVersion = std::mem::uninitialized();

        let var_val = match global.value().code_gen_mu(&mut vm, func_ver) {
            Ok(v) => v,
            Err(e) => panic!("Could not initialize variable.")
        };

        vm.global_vars.insert(var_name.clone(), (var_type, var_val));
    }

    for prototype in nodes.prototypes {
        vm.extern_prototypes
            .insert(prototype.name().clone(), prototype.clone());
        vm.fn_type_map
            .insert(prototype.name().clone(), prototype.return_type().clone());
    }

    for function in nodes.functions.iter_mut() {
        function.code_gen_mu(&mut vm);
    }

    let lasf_function = match &vm.last_runnable_function {
        Some(f) => f,
        _ => {
            results.push(Err(String::from(
                "No runnable function has been defined."
            )));
            return results;
        }
    };

    make_boot_image(&vm, lasf_function, use_llvm_aot);

    let mut deps = vec![];

    for function in vm.functions.keys() {
        deps.push(function.clone());
    }

    for f in 0..vm.curr_function {
        let f = format!("f_anonn_{}", f);
        let f_type = vm.fn_type_map.get(f.as_str()).unwrap();
        results.push(run_code(&vm, &f, f_type, &deps));
    }

    results
}

fn make_boot_image(mu_vm: &Mu_VM, f: &MuEntityHeader, use_llvm_aot: bool) {
    let primordial_func = mu_vm.vm.handle_from_func(f.id());
    let whitelist = mu_vm.whitelist.clone();

    mu_vm.vm.set_primordial_thread(f.id(), true, vec![]);

    if use_llvm_aot {
        mu_aot_llvm::aot_mu::make_boot_image(
            &(*mu_vm.vm),
            whitelist,
            Some(&*primordial_func),
            None,
            None,
            Vec::new(),
            Vec::new(),
            Vec::new(),
            Vec::new(),
            String::from("boot-image.so")
        );
    } else {
        mu_vm.vm.make_boot_image(
            whitelist,
            Some(&*primordial_func),
            None,
            None,
            Vec::new(),
            Vec::new(),
            Vec::new(),
            Vec::new(),
            String::from("boot-image.so")
        );
    }
}

unsafe fn run_code(
    mu_vm: &Mu_VM,
    name: &String,
    f_type: &Type,
    deps: &Vec<String>
) -> Result<RunResult, String> {
    let libname = format!("lib{}.dylib", name);

    let mut dylib_args = vec![Arc::new(name.clone())];

    for dep in deps {
        dylib_args.push(Arc::new(dep.clone()));
    }

    // using boot image from vm.make_boot_image
    let dylib = PathBuf::from("emit/boot-image.so");

    let lib: ll::Library = unsafe {
        std::mem::transmute(
            ll::os::unix::Library::open(
                Some(dylib.as_os_str()),
                libc::RTLD_NOW | libc::RTLD_GLOBAL
            )
            .unwrap()
        )
    };

    unsafe {
        let tl_address = Address::zero();

        MuThread::current_thread_as_mu_thread(tl_address, mu_vm.vm.clone());
    }

    lib.get::<unsafe extern "C" fn(Address)>("set_thread_local".as_bytes())
        .unwrap()(mu::runtime::thread::muentry_get_thread_local());

    unsafe {
        match f_type {
            Type::Double => {
                let function: ll::Symbol<unsafe extern "C" fn() -> f64> =
                    lib.get(name.as_bytes()).unwrap();
                Ok(RunResult::Double(function()))
            }
            Type::Int => {
                let function: ll::Symbol<unsafe extern "C" fn() -> i64> =
                    lib.get(name.as_bytes()).unwrap();
                Ok(RunResult::Int(function()))
            }
            Type::Bool => {
                let function: ll::Symbol<unsafe extern "C" fn() -> bool> =
                    lib.get(name.as_bytes()).unwrap();
                Ok(RunResult::Bool(function()))
            }
            _ => Err("unsupported function type.".parse().unwrap())
        }
    }
}

trait Mu_code_gen {
    unsafe fn code_gen_mu(&mut self, mu_vm: &mut Mu_VM);
}

impl Mu_code_gen for Function {
    unsafe fn code_gen_mu(&mut self, mu_vm: &mut Mu_VM) {
        let runnable = self.proto().name() == "";
        let mut vm = &mut mu_vm.vm;

        let name;
        let function_sig;

        if runnable {
            name = format!("f_anonn_{}", &mu_vm.curr_function);
            let curr_var_type_table = mu_vm.var_type_map.clone();
            let curr_fn_type_table = mu_vm.fn_type_map.clone();

            let function_type = self
                .body()
                .get_type(&curr_var_type_table, &curr_fn_type_table);

            mu_vm
                .fn_type_map
                .insert(name.clone(), function_type.clone());
            mu_vm.curr_function += 1;

            let function_ret_type = mu_vm.get_mu_type(&function_type);
            function_sig = create_mu_sig(
                &mu_vm.vm,
                &name,
                vec![function_ret_type],
                vec![]
            );
        } else {
            name = self.proto().name().clone();
            mu_vm
                .fn_type_map
                .insert(name.clone(), self.proto().return_type().clone());

            let arg_types: Vec<P<MuType>> =
                self.proto()
                    .args()
                    .iter()
                    .map(|(type_par, name_par)| -> P<MuType> {
                        match type_par {
                            Type::IntArray(_) => {
                                mu_vm.get_mu_ref_type(&RefType::IRef(Type::Int))
                            }
                            Type::BoolArray(_) => mu_vm
                                .get_mu_ref_type(&RefType::IRef(Type::Bool)),
                            Type::DoubleArray(_) => mu_vm
                                .get_mu_ref_type(&RefType::IRef(Type::Double)),
                            t => mu_vm.get_mu_type(t)
                        }
                    })
                    .collect();

            let return_type = mu_vm.get_mu_type(self.proto().return_type());
            function_sig =
                create_mu_sig(&mu_vm.vm, &name, vec![return_type], arg_types);
        }

        let function_decl =
            declare_mu_function(&mu_vm.vm, &name, &function_sig);
        mu_vm.whitelist.push(function_decl.id());

        let mut function_version = create_mu_function_ver(
            &mu_vm.vm,
            &name,
            &function_decl,
            &function_sig
        );

        let mut args = vec![];
        let mut form_args = LinkedHashMap::new();
        let mut var_type_map = HashMap::new();
        let mut alloca_instr = vec![];

        for (arg_type, arg_name) in self.proto().args() {
            let form_arg = match arg_type {
                Type::IntArray(_) => function_version.new_ssa(
                    MuEntityHeader::named(
                        mu_vm.vm.next_id(),
                        Arc::new(arg_name.clone())
                    ),
                    mu_vm.get_mu_ref_type(&RefType::IRef(Type::Int))
                ),
                Type::DoubleArray(_) => function_version.new_ssa(
                    MuEntityHeader::named(
                        mu_vm.vm.next_id(),
                        Arc::new(arg_name.clone())
                    ),
                    mu_vm.get_mu_ref_type(&RefType::IRef(Type::Double))
                ),
                Type::BoolArray(_) => function_version.new_ssa(
                    MuEntityHeader::named(
                        mu_vm.vm.next_id(),
                        Arc::new(arg_name.clone())
                    ),
                    mu_vm.get_mu_ref_type(&RefType::IRef(Type::Bool))
                ),
                t => function_version.new_ssa(
                    MuEntityHeader::named(
                        mu_vm.vm.next_id(),
                        Arc::new(arg_name.clone())
                    ),
                    mu_vm.get_mu_type(arg_type)
                )
            };

            mu_vm.vm.set_name(form_arg.as_entity());
            form_args.insert(arg_name.clone(), form_arg.clone());
            var_type_map.insert(arg_name.clone(), arg_type.clone());
            args.push(form_arg.clone_value());
        }

        let mut blk_entry = Block::new(MuEntityHeader::named(
            mu_vm.vm.next_id(),
            Mu("blk_entry")
        ));
        &mu_vm.vm.set_name(blk_entry.as_entity());

        let blk_entry_id = blk_entry.hdr.id();
        mu_vm.curr_block = blk_entry_id;
        mu_vm.blocks_map.insert(
            blk_entry.hdr.id(),
            Mu_Block {
                block: blk_entry,
                instr: alloca_instr,
                block_actual_args: args.clone(),
                args_mapping: form_args.clone(),
                store: form_args.clone(),
                var_type_map: var_type_map.clone(),
                env: args.clone()
            }
        );

        if runnable {
            mu_vm.last_runnable_function = Some(function_decl);
        } else {
            typedef!((mu_vm.vm) type_funcref = mu_funcref(function_sig));
            constdef!((mu_vm.vm) <type_funcref> const_funcref = Constant::FuncRef(function_decl.clone()));

            let func_ref_local = function_version.new_constant(const_funcref);
            mu_vm.functions.insert(name, func_ref_local);
        }

        let value = match self.body().code_gen_mu(mu_vm, &mut function_version)
        {
            Ok(r) => r,
            Err(e) => {
                println!("{}", e);
                return;
            }
        };

        // RET
        inst!((&mu_vm.vm, function_version) blk_entry_ret:
            RET (value)
        );

        let mut curr_block =
            mu_vm.blocks_map.get_mut(&mu_vm.curr_block).unwrap();
        curr_block.instr.push(blk_entry_ret);

        set_block_contents(&mut mu_vm.blocks_map);

        function_version.define(FunctionContent::new(blk_entry_id, {
            let mut ret = LinkedHashMap::new();
            for (block_id, mu_block) in &mu_vm.blocks_map {
                ret.insert(*block_id, mu_block.block.clone());
            }
            ret
        }));

        &mu_vm.vm.define_func_version(function_version);
        mu_vm.blocks_map.clear();
        mu_vm.curr_block = 0;
    }
}

pub fn create_mu_load(
    mu_vm: &mut Mu_VM,
    func_version: &mut MuFunctionVersion,
    mem_loc: P<TreeNode>,
    name: String
) -> (P<TreeNode>, P<TreeNode>) {
    // TODO use proper type
    let var_loaded_name = format!("{}_l", name);
    let var_loaded = func_version.new_ssa(
        MuEntityHeader::named(
            mu_vm.vm.next_id(),
            Arc::new(var_loaded_name.clone())
        ),
        mu_vm.get_mu_type(&Type::Double)
    );
    mu_vm.vm.set_name(var_loaded.as_entity());

    let new_load_instr = func_version.new_inst(Instruction {
        hdr: MuEntityHeader::unnamed(mu_vm.vm.next_id()),
        value: Some(vec![var_loaded.clone_value()]),
        ops: vec![mem_loc],
        v: Instruction_::Load {
            is_ptr: false,
            order: MemoryOrder::SeqCst,
            mem_loc: 0
        }
    });

    (var_loaded, new_load_instr)
}

pub fn create_mu_store(
    mu_vm: &mut Mu_VM,
    func_version: &mut MuFunctionVersion,
    value: P<TreeNode>,
    mem_loc: P<TreeNode>,
    alloca_type: P<MuType>
) -> P<TreeNode> {
    let new_store_instr = func_version.new_inst(Instruction {
        hdr: MuEntityHeader::unnamed(mu_vm.vm.next_id()),
        value: None,
        ops: vec![value, mem_loc],
        v: Instruction_::Store {
            is_ptr: false,
            order: MemoryOrder::SeqCst,
            mem_loc: 1,
            value: 0
        }
    });

    new_store_instr
}

pub fn create_mu_alloca(
    mu_vm: &mut Mu_VM,
    func_version: &mut MuFunctionVersion,
    alloca_type: P<MuType>,
    name: String
) -> (P<TreeNode>, P<TreeNode>) {
    let mem_loc_type = mu_vm.vm.declare_type(
        MuEntityHeader::unnamed(mu_vm.vm.next_id()),
        MuType_::iref(alloca_type.clone())
    );
    mu_vm.vm.set_name(mem_loc_type.as_entity());

    let mem_loc_name = mu_vm.generate_name(format!("loc_{}", name));

    let mem_loc = func_version.new_ssa(
        MuEntityHeader::named(
            mu_vm.vm.next_id(),
            Arc::new(mem_loc_name.clone())
        ),
        mem_loc_type
    );
    mu_vm.vm.set_name(mem_loc.as_entity());

    let new_alloca_inst = func_version.new_inst(Instruction {
        hdr: MuEntityHeader::unnamed(mu_vm.vm.next_id()),
        value: Some(vec![mem_loc.clone_value()]),
        ops: vec![],
        v: Instruction_::AllocA(alloca_type)
    });

    (mem_loc, new_alloca_inst)
}

fn create_mu_function_ver(
    vm: &VM,
    name: &String,
    decl: &MuEntityHeader,
    sig: &P<MuFuncSig>
) -> MuFunctionVersion {
    let name = name.clone().add("_v1");

    let mut function_version = MuFunctionVersion::new(
        MuEntityHeader::named(vm.next_id(), Arc::new(name)),
        decl.id(),
        sig.clone()
    );

    vm.set_name(function_version.as_entity());

    function_version
}

fn declare_mu_function(
    vm: &VM,
    name: &String,
    sig: &P<MuFuncSig>
) -> MuEntityHeader {
    let func: MuFunction = MuFunction::new(
        MuEntityHeader::named(vm.next_id(), Arc::new(name.clone())),
        sig.clone()
    );
    vm.set_name(func.as_entity());
    let ccall_exit = func.hdr.clone();
    vm.declare_func(func);

    ccall_exit
}

fn create_mu_sig(
    vm: &VM,
    name: &String,
    ret_tys: Vec<P<MuType>>,
    arg_tys: Vec<P<MuType>>
) -> P<MuFuncSig> {
    let name = name.clone().add("_sig");

    let annon_function_sig = vm.declare_func_sig(
        MuEntityHeader::named(vm.next_id(), Arc::new(name)),
        ret_tys,
        arg_tys
    );

    vm.set_name(annon_function_sig.as_entity());

    annon_function_sig
}

pub fn create_cmp_op_instruction(
    function_version: &mut MuFunctionVersion,
    mu_vm: &mut Mu_VM,
    var: &P<TreeNode>,
    ops: Vec<P<TreeNode>>,
    op: CmpOp
) -> P<TreeNode> {
    function_version.new_inst(Instruction {
        hdr: MuEntityHeader::unnamed(mu_vm.vm.next_id()),
        value: Some(vec![var.clone_value()]),
        ops,
        v: Instruction_::CmpOp(op, 0, 1)
    })
}

pub fn create_int_cmp_op_instruction_eq(
    function_version: &mut MuFunctionVersion,
    mu_vm: &mut Mu_VM,
    var: &P<TreeNode>,
    ops: Vec<P<TreeNode>>
) -> P<TreeNode> {
    function_version.new_inst(Instruction {
        hdr: MuEntityHeader::unnamed(mu_vm.vm.next_id()),
        value: Some(vec![var.clone_value()]),
        ops,
        v: Instruction_::CmpOp(mu::ast::op::CmpOp::EQ, 0, 1)
    })
}

fn set_block_contents(blocks_map: &mut LinkedHashMap<MuID, Mu_Block>) {
    for (block_id, mu_block) in blocks_map.iter_mut() {
        mu_block.block.content = Some(BlockContent {
            args: mu_block.block_actual_args.clone(),
            exn_arg: None,
            body: mu_block.instr.clone(),
            keepalives: None
        });
    }
    //
    //    blk_entry.content = Some(BlockContent {
    //        args,
    //        exn_arg: None,
    //        body: mu_vm.instr.clone(),
    //        keepalives: None,
    //    });
}
