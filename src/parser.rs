use crate::ast;
use crate::ast::Type::{Bool, Double};
use crate::ast::*;
use crate::lexer;
use crate::lexer::Token;
use std::ops::Deref;

#[derive(Clone)]
pub struct ASTNodes {
    pub functions: Vec<Function>,
    pub prototypes: Vec<Prototype>,
    pub global_vars: Vec<Global>
}

pub fn parse_program(tokens: Vec<Token>) -> ASTNodes {
    let mut curr_token = 0;

    let mut nodes = ASTNodes {
        functions: vec![],
        prototypes: vec![],
        global_vars: vec![]
    };

    while curr_token < tokens.len() {
        match &tokens[curr_token] {
            Token::EOF => return nodes,
            Token::CHAR(';') => {
                curr_token += 1;
            }
            Token::DEF => {
                let definition = parse_definition(&tokens, &mut curr_token);
                if let Some(function) = definition {
                    println!("{}", function.print());
                    nodes.functions.push(*function);
                }
            }
            Token::GLOBAL => {
                let global = parse_global(&tokens, &mut curr_token);
                if let Some(var) = global {
                    println!("{}", var.print());
                    nodes.global_vars.push(*var);
                }
            }
            Token::EXTERN => {
                let prototype = parse_extern(&tokens, &mut curr_token);
                if let Some(prototype) = prototype {
                    println!("{}", prototype.print());
                    nodes.prototypes.push(*prototype);
                }
            }
            _ => {
                let top_level_exp =
                    parse_top_level_expression(&tokens, &mut curr_token);
                if let Some(function) = top_level_exp {
                    println!("{}", function.print());
                    nodes.functions.push(*function);
                }
            }
        }
    }

    nodes
}

fn parse_global(
    tokens: &Vec<Token>,
    curr_token: &mut usize
) -> Option<Box<Global>> {
    // skip GLOBAL token
    *curr_token += 1;

    let var_type = match &tokens[*curr_token] {
        Token::BOOL => Type::Bool,
        Token::INT => Type::Int,
        Token::DOUBLE => Type::Double,
        Token::BOOL_ARRAY => Type::BoolArray(-1),
        Token::INT_ARRAY => Type::IntArray(-1),
        Token::DOUBLE_ARRAY => Type::DoubleArray(-1),
        _ => return None
    };

    *curr_token += 1;

    let var_name = match &tokens[*curr_token] {
        Token::ID(id) => id.clone(),
        _ => return None
    };
    *curr_token += 1;

    let mut assigned_var = false;
    match &tokens[*curr_token] {
        Token::CHAR('=') => {
            *curr_token += 1;
            assigned_var = true;
        }
        _ => ()
    }

    if assigned_var {
        let assigned_exp = match parse_expression(&tokens, curr_token) {
            Some(exp) => exp,
            None => return None
        };
        Some(Box::new(Global::new(
            Var::new(var_name),
            var_type,
            assigned_exp
        )))
    } else {
        let default_exp: Box<dyn Exp> = match var_type {
            Type::Double => Box::new(RealLiteral::new(0.0)),
            Type::Bool => Box::new(BoolLiteral::new(false)),
            Type::Int => Box::new(IntLiteral::new(0)),
            _ => panic!("Variable must be explicitly initialized.")
        };
        Some(Box::new(Global::new(
            Var::new(var_name),
            var_type,
            default_exp
        )))
    }
}

fn parse_definition(
    tokens: &Vec<Token>,
    curr_token: &mut usize
) -> Option<Box<Function>> {
    // skip DEF token
    *curr_token += 1;

    let prototype = match parse_prototype(&tokens, curr_token) {
        Some(prototype) => prototype,
        None => return None
    };

    let exp = match parse_expression(&tokens, curr_token) {
        Some(exp) => exp,
        None => return None
    };

    Some(Box::new(Function::new(prototype, exp)))
}

fn parse_extern(
    tokens: &Vec<Token>,
    curr_token: &mut usize
) -> Option<Box<Prototype>> {
    // skip extern token
    *curr_token += 1;

    parse_prototype(&tokens, curr_token)
}

fn parse_top_level_expression(
    tokens: &Vec<Token>,
    curr_token: &mut usize
) -> Option<Box<Function>> {
    let exp = match parse_expression(&tokens, curr_token) {
        Some(exp) => exp,
        None => return None
    };

    let prototype = Prototype::new("".to_string(), Type::Annon, vec![]);

    Some(Box::new(Function::new(Box::new(prototype), exp)))
}

fn parse_prototype(
    tokens: &Vec<Token>,
    curr_token: &mut usize
) -> Option<Box<Prototype>> {
    // TODO allow functions to return arrays
    let proto_ret_type = match &tokens[*curr_token] {
        Token::BOOL => Type::Bool,
        Token::INT => Type::Int,
        Token::DOUBLE => Type::Double,

        _ => return None
    };

    *curr_token += 1;

    let proto_name = match &tokens[*curr_token] {
        Token::ID(s) => s,
        _ => return None
    };

    *curr_token += 1;

    match &tokens[*curr_token] {
        Token::CHAR('(') => (),
        _ => return None
    }

    let mut proto_args = vec![];

    loop {
        let mut arg_name = "".to_string();
        let arg_type;

        *curr_token += 1;
        arg_type = match &tokens[*curr_token] {
            Token::BOOL => Type::Bool,
            Token::INT => Type::Int,
            Token::DOUBLE => Type::Double,
            Token::BOOL_ARRAY => Type::BoolArray(-1),
            Token::INT_ARRAY => Type::IntArray(-1),
            Token::DOUBLE_ARRAY => Type::DoubleArray(-1),
            _ => break
        };

        *curr_token += 1;
        arg_name = match &tokens[*curr_token] {
            Token::ID(id) => id.clone(),
            _ => break
        };

        proto_args.push((arg_type, arg_name));
    }

    match &tokens[*curr_token] {
        Token::CHAR(')') => *curr_token += 1,
        _ => return None
    }

    Some(Box::new(Prototype::new(
        proto_name.clone(),
        proto_ret_type.clone(),
        proto_args
    )))
}

fn parse_expression(
    tokens: &Vec<Token>,
    curr_token: &mut usize
) -> Option<Box<dyn Exp>> {
    let lhs = match parse_primary(&tokens, curr_token) {
        Some(exp) => exp,
        None => return None
    };

    parse_bin_op_rhs(&tokens, curr_token, lhs, 0)
}

fn parse_primary(
    tokens: &Vec<Token>,
    curr_token: &mut usize
) -> Option<Box<dyn Exp>> {
    match &tokens[*curr_token] {
        Token::ID(id) => parse_identifier_exp(&tokens, curr_token),
        Token::REAL_LIT(_) | Token::INT_LIT(_) | Token::BOOL_LIT(_) => {
            parse_number_exp(&tokens, curr_token)
        }
        Token::CHAR('(') => parse_parenthesized_exp(&tokens, curr_token),
        Token::CHAR('{') => parse_array_exp(&tokens, curr_token),
        Token::IF => parse_conditional_exp(&tokens, curr_token),
        Token::FOR => parse_for_exp(&tokens, curr_token),
        Token::VAR => parse_var_exp(&tokens, curr_token),
        _ => {
            *curr_token += 1;
            None
        }
    }
}

fn parse_var_exp(
    tokens: &Vec<Token>,
    curr_token: &mut usize
) -> Option<Box<dyn Exp>> {
    if let Token::VAR = &tokens[*curr_token] {
        *curr_token += 1;

        let mut vars = vec![];

        loop {
            let var;

            let var_type = match &tokens[*curr_token] {
                Token::BOOL => Type::Bool,
                Token::INT => Type::Int,
                Token::DOUBLE => Type::Double,
                Token::BOOL_ARRAY => Type::BoolArray(-1),
                Token::INT_ARRAY => Type::IntArray(-1),
                Token::DOUBLE_ARRAY => Type::DoubleArray(-1),
                _ => break
            };

            *curr_token += 1;

            match &tokens[*curr_token] {
                Token::ID(id) => {
                    var = Var::new(id.clone());
                    *curr_token += 1;
                }
                _ => return None
            }

            let mut assigned_var = false;
            match &tokens[*curr_token] {
                Token::CHAR('=') => {
                    *curr_token += 1;
                    assigned_var = true;
                }
                _ => ()
            }

            if assigned_var {
                let assigned_exp = match parse_expression(&tokens, curr_token) {
                    Some(exp) => exp,
                    None => return None
                };

                vars.push((var_type, var, assigned_exp));
            } else {
                let default_exp: Box<dyn Exp> = match var_type {
                    Type::Double => Box::new(RealLiteral::new(0.0)),
                    Type::Bool => Box::new(BoolLiteral::new(false)),
                    Type::Int => Box::new(IntLiteral::new(0)),
                    _ => panic!("Variable must be explicitly initialized.")
                };
                vars.push((var_type, var, default_exp));
            }

            match &tokens[*curr_token] {
                Token::IN => {
                    break;
                }
                Token::CHAR(',') => {
                    *curr_token += 1;
                    continue;
                }
                _ => return None
            }
        }

        *curr_token += 1;

        let body_exp = match parse_expression(&tokens, curr_token) {
            Some(exp) => exp,
            None => return None
        };

        Some(Box::new(VarExp::new(vars, body_exp)))
    } else {
        None
    }
}

fn parse_for_exp(
    tokens: &Vec<Token>,
    curr_token: &mut usize
) -> Option<Box<dyn Exp>> {
    if let Token::FOR = &tokens[*curr_token] {
        *curr_token += 1;

        let var_type = match &tokens[*curr_token] {
            Token::BOOL => Type::Bool,
            Token::INT => Type::Int,
            Token::DOUBLE => Type::Double,
            Token::BOOL_ARRAY => Type::BoolArray(-1),
            Token::INT_ARRAY => Type::IntArray(-1),
            Token::DOUBLE_ARRAY => Type::DoubleArray(-1),
            _ => return None
        };

        *curr_token += 1;

        let var;
        match &tokens[*curr_token] {
            Token::ID(id) => {
                var = Var::new(id.clone());
                *curr_token += 1;
            }
            _ => return None
        }

        match &tokens[*curr_token] {
            Token::CHAR('=') => (),
            _ => return None
        }
        *curr_token += 1;

        let start_exp = match parse_expression(&tokens, curr_token) {
            Some(exp) => exp,
            None => return None
        };

        match &tokens[*curr_token] {
            Token::CHAR(',') => (),
            _ => return None
        }
        *curr_token += 1;

        let end_exp = match parse_expression(&tokens, curr_token) {
            Some(exp) => exp,
            None => return None
        };

        let mut step_exp: Box<dyn Exp> = Box::new(RealLiteral::new(1.0));
        let mut default_step = false;

        match &tokens[*curr_token] {
            Token::CHAR(',') => (),
            Token::IN => {
                default_step = true;
            }
            _ => return None
        }

        *curr_token += 1;

        if !default_step {
            step_exp = match parse_expression(&tokens, curr_token) {
                Some(exp) => exp,
                None => return None
            };

            match &tokens[*curr_token] {
                Token::IN => (),
                _ => return None
            }
            *curr_token += 1;
        }

        let body_exp = match parse_expression(&tokens, curr_token) {
            Some(exp) => exp,
            None => return None
        };

        Some(Box::new(ForExp::new(
            var, var_type, start_exp, end_exp, step_exp, body_exp
        )))
    } else {
        None
    }
}

fn parse_conditional_exp(
    tokens: &Vec<Token>,
    curr_token: &mut usize
) -> Option<Box<dyn Exp>> {
    if let Token::IF = &tokens[*curr_token] {
        *curr_token += 1;

        let cond = match parse_expression(&tokens, curr_token) {
            Some(exp) => exp,
            None => return None
        };

        match &tokens[*curr_token] {
            Token::THEN => (),
            _ => return None
        }
        *curr_token += 1;

        let then_branch = match parse_expression(&tokens, curr_token) {
            Some(exp) => exp,
            None => return None
        };

        match &tokens[*curr_token] {
            Token::ELSE => (),
            _ => return None
        }
        *curr_token += 1;

        let else_branch = match parse_expression(&tokens, curr_token) {
            Some(exp) => exp,
            None => return None
        };

        Some(Box::new(ConditionalExp::new(
            cond,
            then_branch,
            else_branch
        )))
    } else {
        None
    }
}

fn parse_identifier_exp(
    tokens: &Vec<Token>,
    curr_token: &mut usize
) -> Option<Box<dyn Exp>> {
    match &tokens[*curr_token] {
        Token::ID(id) => {
            *curr_token += 1;

            match &tokens[*curr_token] {
                Token::CHAR('(') => {
                    *curr_token += 1;

                    let mut call_args = vec![];

                    loop {
                        match &tokens[*curr_token] {
                            &Token::CHAR(')') => {
                                *curr_token += 1;
                                break;
                            }
                            &Token::CHAR(',') => {
                                *curr_token += 1;
                                continue;
                            }
                            _ => ()
                        };

                        if *curr_token >= tokens.len() {
                            return None;
                        }

                        let exp = match parse_expression(&tokens, curr_token) {
                            Some(exp) => exp,
                            None => return None
                        };

                        call_args.push(exp);
                    }

                    Some(Box::new(CallExp::new(id.clone(), call_args)))
                }
                Token::CHAR('[') => {
                    *curr_token += 1;

                    let exp = match parse_expression(&tokens, curr_token) {
                        Some(exp) => exp,
                        None => return None
                    };

                    match &tokens[*curr_token] {
                        &Token::CHAR(']') => {
                            *curr_token += 1;
                        }
                        _ => return None
                    }

                    Some(Box::new(ArrayAccess::new(id.clone(), exp)))
                }
                _ => return Some(Box::new(Var::new(id.clone())))
            }
        }
        _ => return None
    }
}

fn parse_number_exp(
    tokens: &Vec<Token>,
    curr_token: &mut usize
) -> Option<Box<dyn Exp>> {
    if let Token::REAL_LIT(v) = &tokens[*curr_token] {
        *curr_token += 1;

        Some(Box::new(RealLiteral::new(*v)))
    } else if let Token::INT_LIT(v) = &tokens[*curr_token] {
        *curr_token += 1;

        Some(Box::new(IntLiteral::new(*v)))
    } else if let Token::BOOL_LIT(v) = &tokens[*curr_token] {
        *curr_token += 1;

        Some(Box::new(BoolLiteral::new(*v)))
    } else {
        None
    }
}

fn parse_array_exp(
    tokens: &Vec<Token>,
    curr_token: &mut usize
) -> Option<Box<dyn Exp>> {
    if let Token::CHAR('{') = &tokens[*curr_token] {
        *curr_token += 1;

        let mut exps = vec![];

        loop {
            let exp = match parse_expression(&tokens, curr_token) {
                Some(exp) => exp,
                None => return None
            };

            exps.push(exp);

            match &tokens[*curr_token] {
                Token::CHAR(',') => *curr_token += 1,
                _ => break
            }
        }

        match &tokens[*curr_token] {
            Token::CHAR('}') => (),
            _ => return None
        }
        *curr_token += 1;

        Some(Box::new(ArrayLiteral::new(exps)))
    } else {
        None
    }
}

fn parse_parenthesized_exp(
    tokens: &Vec<Token>,
    curr_token: &mut usize
) -> Option<Box<dyn Exp>> {
    if let Token::CHAR('(') = &tokens[*curr_token] {
        *curr_token += 1;

        let exp = match parse_expression(&tokens, curr_token) {
            Some(exp) => exp,
            None => return None
        };

        match &tokens[*curr_token] {
            Token::CHAR(')') => (),
            _ => return None
        }
        *curr_token += 1;

        Some(exp)
    } else {
        None
    }
}

fn parse_bin_op_rhs(
    tokens: &Vec<Token>,
    curr_token: &mut usize,
    lhs: Box<dyn Exp>,
    prec: i32
) -> Option<Box<dyn Exp>> {
    let curr_lhs = lhs;

    if let Token::CHAR(op1) = &tokens[*curr_token] {
        let op1_precedence = get_precedence(op1);
        if op1_precedence < prec {
            return Some(curr_lhs);
        };

        *curr_token += 1;

        let mut rhs = match parse_primary(&tokens, curr_token) {
            Some(exp) => exp,
            None => return None
        };

        match &tokens[*curr_token] {
            Token::CHAR(op2) => {
                let op2_precedence = get_precedence(op2);
                if op1_precedence < op2_precedence {
                    rhs = match parse_bin_op_rhs(
                        &tokens,
                        curr_token,
                        rhs,
                        op1_precedence
                    ) {
                        Some(exp) => exp,
                        None => return None
                    };
                }
            }
            _ => ()
        }

        let new_lhs = Box::new(BinaryExp::new(*op1, curr_lhs, rhs));

        parse_bin_op_rhs(&tokens, curr_token, new_lhs, prec)
    } else {
        Some(curr_lhs)
    }
}

fn get_precedence(c: &char) -> i32 {
    match c {
        '<' => 10,
        '+' => 20,
        '-' => 20,
        '*' => 40,
        '=' => 0,
        _ => -1
    }
}
