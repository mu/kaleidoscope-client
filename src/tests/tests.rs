use crate::generator;
use crate::generator::RunResult;
use crate::lexer;
use crate::mu_generator;
use crate::parser;

#[test]
fn test_double_literal() {
    let input = "42.0";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_double_literal\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_double_literal\0"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_double_literal\0"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> =
        vec![Ok(RunResult::Double(42.0))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_int_literal() {
    let input = "42";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_int_literal\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_int_literal\0"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_int_literal\0"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> = vec![Ok(RunResult::Int(42))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_bool_literal() {
    let input = "true";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_bool_literal\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_bool_literal\0"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_bool_literal\0"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> =
        vec![Ok(RunResult::Bool(true))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_multiple_exps() {
    let input = "14.0-2.0 + 3.0*2.0*5.0; 12.0 + 6.0*5.0";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_multiple_exps\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_multiple_exps\0"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_multiple_exps\0"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> =
        vec![Ok(RunResult::Double(42.0)), Ok(RunResult::Double(42.0))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_double_function_call() {
    let input = "def double foo(double x double y) x*y; foo(3.0 2.0);";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_double_function_call\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_double_function_call\0"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_double_function_call\0"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> =
        vec![Ok(RunResult::Double(6.0))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

fn test_int_function_call() {
    let input = "def int foo(int x int y) x*y; foo(3 2);";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_double_function_call\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_double_function_call\0"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_double_function_call\0"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> = vec![Ok(RunResult::Int(6))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_extern_function_call() {
    let input = "extern double sin(double a); sin(1.23)";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_extern_function_call\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_extern_function_call\0"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_extern_function_call\0"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> =
        vec![Ok(RunResult::Double(0.9424888019316975))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_simple_if() {
    let input = "if 2.0 < 3.0 then 1.0 else 2.0";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_simple_if\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_simple_if\0"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_simple_if\0"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> =
        vec![Ok(RunResult::Double(1.0))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_simple_if_true() {
    let input = "if true then 1.0 else 2.0";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_simple_if_bool\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_simple_if_bool\0"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_simple_if_bool\0"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> =
        vec![Ok(RunResult::Double(1.0))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_simple_if_else_case() {
    let input = "if 4.0 < 3.0 then 1.0 else 2.0";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_simple_if_else_case\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_simple_if_else_case\0"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_simple_if_else_case\0"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> =
        vec![Ok(RunResult::Double(2.0))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_fibonacci() {
    let input =
        "def double fib(double x) if x < 3.0 then 1.0 else fib(x-1.0)+fib(x-2.0); fib(10.0)";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_fibonacci\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_fibonacci\0"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_fibonacci\0"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> =
        vec![Ok(RunResult::Double(55.0))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_fibonacci_int() {
    let input =
        "def int fib(int x) if x < 3 then 1 else fib(x-1)+fib(x-2); fib(10)";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_fibonacci\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_fibonacci\0"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_fibonacci\0"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> = vec![Ok(RunResult::Int(55))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_for_loop_no_mutable() {
    let input = "def double foo(double x) for double i = 1.0, i < x, 1.0 in x+1.0; foo(5.0)";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_for_loop_no_mutable\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_for_loop_no_mutable\0"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_for_loop_no_mutable\0"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> =
        vec![Ok(RunResult::Double(0.0))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_mutable_assignment_binary_op() {
    let input =
        "def double seq (double x double y) y; def double foo(double x) seq(x = 10.0 x) foo(4.0)";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_mutable_assignment_binary_op\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_mutable_assignment_binary_op"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_mutable_assignment_binary_op"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> =
        vec![Ok(RunResult::Double(10.0))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_int_mutable_assignment_binary_op() {
    let input =
        "def int seq (int x int y) y; def int foo(int x) seq(x = 10 x) foo(4)";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_int_mutable_assignment_binary_op\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_int_mutable_assignment_binary_op"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_int_mutable_assignment_binary_op"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> = vec![Ok(RunResult::Int(10))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_int_mutable_var_plus_one_function_with_assignment_expr() {
    let input = "def int plusone(int x) var int y = x in y + 1 plusone(3)";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from(
                "test_int_mutable_var_plus_one_function_with_assignment_expr\0"
            ),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from(
                "test_int_mutable_var_plus_one_function_with_assignment_expr"
            ),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from(
                "test_int_mutable_var_plus_one_function_with_assignment_expr"
            ),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> = vec![Ok(RunResult::Int(4))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_mutable_var_plus_one_function_with_assignment_expr() {
    let input =
        "def double plusone(double x) var double y = x in y + 1.0 plusone(3.0)";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from(
                "test_mutable_var_plus_one_function_with_assignment_expr\0"
            ),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_mutable_var_assignment_exp"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_mutable_var_assignment_exp"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> =
        vec![Ok(RunResult::Double(4.0))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_mutable_pow_function() {
    let input = "def double seq(double x double y) y; def double pow(double x double y) var double total = x in seq(for double i = 1.0, i < y in total = total * x total ) pow (3.0 4.0); pow (2.0 4.0) ";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_mutable_pow_function\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_mutable_var_assignment_exp"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_mutable_var_assignment_exp"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> =
        vec![Ok(RunResult::Double(81.0)), Ok(RunResult::Double(16.0))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_int_mutable_pow_function() {
    let input = "def int seq(double x int y)
   y

def int pow(int x double y)
  var int total = x in
    seq(for double i = 1.0, i < y in
          total = total * x
        total )

pow (3 4.0); pow (2 4.0)";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_int_mutable_pow_function\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_int_mutable_pow_function"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_int_mutable_pow_function"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> =
        vec![Ok(RunResult::Int(81)), Ok(RunResult::Int(16))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_mutable_fibonacci_iter() {
    let input = "def double seq(double x double y) y; def double fibi(double x) var double a = 1.0, double b = 1.0, double c in seq (( for double i = 2.0, i < x in seq (c = a + b  seq (a = b b = c))) b) ; fibi(10.0)";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_mutable_fibonacci_iter\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_mutable_var_assignment_exp"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_mutable_var_assignment_exp"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> =
        vec![Ok(RunResult::Double(55.0))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_int_mutable_fibonacci_iter() {
    let input = "def int seq1(double x int y)
           y

        def int seq(int x int y)
           y

        def int fibi(double x)
          var int a = 1, int b = 1, int c = 0 in
          seq1 (( for double i = 2.0, i < x in
               seq (c = a + b  seq (a = b b = c)))
               b) ;

        fibi(10.0)";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_int_mutable_fibonacci_iter\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_int_mutable_fibonacci_iter"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_int_mutable_fibonacci_iter"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> = vec![Ok(RunResult::Int(55))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_array_access_variable() {
    let input = "var int[] x = {12, 232, 33} in x[2]";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_array_access_variable\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_array_access_variable"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_int_mutable_fibonacci_iter"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> = vec![Ok(RunResult::Int(33))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_array_access_variable_function() {
    let input = "def int foo(int[] x)
  x[1]

var int[] x = {13, 25, 32} in
    foo(x)";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_array_access_variable_function\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_array_access_variable_function"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_array_access_variable_function"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> = vec![Ok(RunResult::Int(25))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_global_variable() {
    let input = "global int x = 42;

def int foo()
  x

foo()";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_global_variable\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_global_variable"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_global_variable"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> = vec![Ok(RunResult::Int(42))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}

#[test]
fn test_global_variable_mutable() {
    let input = "global int x = 42;

def int seq (int x int y) y

def int foo()
  seq(x=1 x)

foo()";
    let tokens = lexer::tokenize(String::from(input));
    let nodes = parser::parse_program(tokens);
    let llvm_values;
    let mu_values;
    let mu_llvm_values;
    unsafe {
        llvm_values = generator::code_gen(
            String::from("test_global_variable_mutable\0"),
            nodes.clone()
        );
        mu_values = mu_generator::code_gen(
            String::from("test_global_variable"),
            nodes.clone(),
            false,
            false
        );
        mu_llvm_values = mu_generator::code_gen(
            String::from("test_global_variable"),
            nodes.clone(),
            true,
            false
        );
    }

    let expected: Vec<Result<RunResult, String>> = vec![Ok(RunResult::Int(1))];
    assert_eq!(llvm_values, expected);
    assert_eq!(mu_values, expected);
    assert_eq!(mu_llvm_values, expected);
}
